package com.nano.mobisir.merchant.nanoMainView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nano.mobisir.merchant.R;
import com.nano.mobisir.merchant.nanoadapter.CustomAdapter;
import com.nano.mobisir.merchant.nanologin.login;
import com.nano.mobisir.merchant.network.Connection_detector;
import com.nano.mobisir.merchant.network.NanoUrlDetails;
import com.nano.mobisir.merchant.network.ServiceHandler;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdul on 1/12/15.
 */
public class Investments extends Activity {

    ListView lv;
    Context context;
    ProgressDialog pDialog;
    TextView mynanoinvest,settings,notifications;

    ArrayList prgmName;
    public static String [] dates=new String[25];
    public static String [] amount=new String[25];
    public static String [] npv=new String[25];
    public static int [] status=new int[25];
    public static int [] detail=new int[25];
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.investment);
//
//        setContentView(R.layout.mynanoinverstinfo);
//        mynanoinvest = (TextView) findViewById(R.id.txt_mynanoinvest);
//        settings = (TextView) findViewById(R.id.txt_settings);
//        notifications = (TextView) findViewById(R.id.txt_notifications);
        context=this;
        System.out.println("0000000000000");

        getNotificationFromServer();


        lv=(ListView) findViewById(R.id.listView);
        lv.setAdapter(new CustomAdapter(Investments.this, dates, amount, npv, status, detail));

        lv.invalidateViews();
        lv.refreshDrawableState();


    }
    private void getNotificationFromServer() {
        //connection detections
        Connection_detector cd = new Connection_detector(getApplicationContext());
        //Internet connection status
        Boolean isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            System.out.println("111111111111111111");
            new PopulateNotification().execute();

        } else {

            Toast.makeText(Investments.this, "Network Not Connected", Toast.LENGTH_LONG).show();
        }


    }



    private class PopulateNotification extends AsyncTask<String, Void, String> {
        Boolean server_Connection_Flag=false;
        //  ProgressDialog pDialog;
        public String jsonStr=null;

        private Context context;
        String email=null;
        String pass=null;


        public PopulateNotification()
        {

        }
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pDialog = new ProgressDialog(Investments.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        //doInBackground method called to do request & get response from server
        @Override
        protected String doInBackground(String... urls) {

            SharedPreferences sharedpreferences = getSharedPreferences(login.MyPREFERENCES, Context.MODE_PRIVATE);
            String merchanttoken = (sharedpreferences.getString("merchanttoken", ""));
            String merchantid = (sharedpreferences.getString("merchantid", ""));
            System.out.println(merchanttoken + "bbbbbbbbbbbbbbbbbbbbb" + merchantid);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("merchantid", merchantid));
            params.add(new BasicNameValuePair("merchantToken", merchanttoken));


            ServiceHandler sh=new ServiceHandler();

            String login_url= NanoUrlDetails.merchantdetail_url;
            try {
                System.out.println("" +
                        "");

                jsonStr = sh.makeServiceCallNotification(login_url,ServiceHandler.POST,params);
//
                System.out.println("getvalues..........."+jsonStr);

//                JSONObject jsonObj = new JSONObject(jsonStr);


//                if(!jsonObj.getString("token").equals("error"))
//                {
//                    Log.e("Response:1", jsonObj.getString("token"));
//                }
//                else
//                {
//                    Log.e("Response:2", jsonObj.getString("error"));
//                }

            } catch (Throwable e) {
            }

            System.out.println("values............"+jsonStr);
            loadTable(jsonStr);



            return jsonStr;

//            return result;
        }

        @Override
        protected void  onPostExecute(String result){
            super.onPostExecute(result);

            System.out.println("zzzzzzzzzzzzzzzzzzzzzz"+result);
            //saveInDatabase();
            Toast mymsg=Toast.makeText(Investments.this, "Transaction details" + result, Toast.LENGTH_LONG);
            mymsg.show();



            if (jsonStr.equals("Server_not_connected")) {

                server_Connection_Flag = false;
            }
            else if (jsonStr.equals("Invalid_User")) {
                System.out.println("invalied user" + jsonStr);

            }
            else if (jsonStr != null) {



//                Intent i = new Intent(myNanoInvestInfo.this, NotificationActivity.class);
////                i.putExtra("loginkey_id",jsonStr);
//                startActivity(i);

            }
            else {
                System.out.println("ServiceHandler Couldn't get any data from the url");
            }

            //dismiss process bar
            if (pDialog.isShowing())
                pDialog.dismiss();

            Log.e("server_Connection_Flag", server_Connection_Flag + "");

            if(server_Connection_Flag){




            }else{
                server_Connection_Flag = true;
                if(jsonStr.equals("Server_not_connected")){

                    //showalert

                    System.out.println("ServiceHandler Couldn't get any data from the url");
                }else if(jsonStr.equals("user_is_not_present")||jsonStr.equals("password_not_matching")){

                    // showAlertDialog

                }else{

                }

            }
        }
    }


    private void loadTable(String Json)
    {
        System.out.println("loadtable.............");

        JSONArray jsonarray = null;
        try {
            jsonarray = new JSONArray(Json);


            for(int i=0; i<jsonarray.length(); i++){
                System.out.println("for..............");
                JSONObject obj = jsonarray.getJSONObject(i);

                dates[i] = obj.getString("transactionTime");
                amount[i] = obj.getString("amount");
                npv[i] = obj.getString("emailid");
                status[i]=R.drawable.down;
                detail[i]=R.drawable.bar;


//
//                status[i]=R.drawable.down;
//                detail[i]=R.drawable.bar;


            }



        } catch (JSONException e) {
            e.printStackTrace();
        }



    }




}
