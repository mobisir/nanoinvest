package com.nano.mobisir.merchant.nanoMainView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.nano.mobisir.merchant.R;
import com.nano.mobisir.merchant.nanolessons.Start;

/*
MainActivity for nanoinvest
 */

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
                startActivity(new Intent(MainActivity.this, Start.class));
            }
        }, 3000);
    }

    protected void onStart(){
      super.onStart();

    }

    protected void onRestart(){
        super.onRestart();

    }

    protected void onResume(){
        super.onResume();

    }

    protected void onPause(){
        super.onPause();

    }

    protected void onStop(){
        super.onPause();

    }

    protected void onDestroy(){
        super.onDestroy();

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
