package com.nano.mobisir.merchant.nanoadapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nano.mobisir.merchant.nanonotifications.mynanoinvest;
import com.nano.mobisir.merchant.nanonotifications.notifications;
import com.nano.mobisir.merchant.nanosettings.settings;

/**
 * Created by abdul on 23/11/15.
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                mynanoinvest tab1 = new mynanoinvest();
                return tab1;
            case 1:
                settings tab2 = new settings();
                return tab2;
            case 2:
                notifications tab3 = new notifications();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}