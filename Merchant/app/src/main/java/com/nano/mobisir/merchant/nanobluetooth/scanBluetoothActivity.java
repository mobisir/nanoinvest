package com.nano.mobisir.merchant.nanobluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nano.mobisir.merchant.R;

import java.io.OutputStream;


public class scanBluetoothActivity extends Activity {

    private static final int REQUEST_ENABLE_BT = 1;
    OutputStream outStream=null;
    ListView listDevicesFound;
    Button btnScanDevice;
    TextView stateBluetooth;
    BluetoothAdapter bluetoothAdapter;
    BluetoothAdapter btAdapter;
    private BluetoothSocket btSocket;

    ArrayAdapter<String> btArrayAdapter;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scanbluetooth);

        btnScanDevice = (Button)findViewById(R.id.scandevice);

        stateBluetooth = (TextView)findViewById(R.id.bluetoothstate);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        listDevicesFound = (ListView)findViewById(R.id.devicesfound);
        btArrayAdapter = new ArrayAdapter<String>(scanBluetoothActivity.this, android.R.layout.simple_list_item_1);
        listDevicesFound.setAdapter(btArrayAdapter);

        CheckBlueToothState();

        btnScanDevice.setOnClickListener(btnScanDeviceOnClickListener);

        registerReceiver(ActionFoundReceiver,
                new IntentFilter(BluetoothDevice.ACTION_FOUND));

        listDevicesFound.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // mBluetoothAdapter.getRemoteDevice(device.getAddress()));
                String info = ((TextView) view).getText().toString();
                String address = info.substring(info.length() - 17);

                // ListView Clicked item index
                int itemPosition     = position;

                // ListView Clicked item value
                String itemValue    = (String) listDevicesFound.getItemAtPosition(position);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :" + info + "  ListItem : " + address, Toast.LENGTH_LONG)
                        .show();
//                sendFile(address,itemValue);


            }

        });
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        unregisterReceiver(ActionFoundReceiver);
    }

    private void CheckBlueToothState(){
        if (bluetoothAdapter == null){
            stateBluetooth.setText("Bluetooth NOT support");
        }else{
            if (bluetoothAdapter.isEnabled()){
                if(bluetoothAdapter.isDiscovering()){
                    stateBluetooth.setText("Bluetooth is currently in device discovery process.");
                }else{
                    stateBluetooth.setText("Bluetooth is Enabled.");
                    btnScanDevice.setEnabled(true);
                }
            }else{
                stateBluetooth.setText("Bluetooth is NOT Enabled!");
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private Button.OnClickListener btnScanDeviceOnClickListener
            = new Button.OnClickListener(){

        @Override
        public void onClick(View arg0) {
            // TODO Auto-generated method stub
            btArrayAdapter.clear();
            bluetoothAdapter.startDiscovery();
        }};

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if(requestCode == REQUEST_ENABLE_BT){
            CheckBlueToothState();
        }
    }

    private final BroadcastReceiver ActionFoundReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            String action = intent.getAction();
//            if(BluetoothDevice.ACTION_FOUND.equals(action)) {
            System.out.println("haiii");
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                btArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                btArrayAdapter.notifyDataSetChanged();
//            }
        }};

//
//
//    public String sendFile(String mac_address, String device_name) {
//        // outStream = new OutputStream();
//        // TODO Auto-generated method stub
//        String stringToSend="data from client";
//        String result="";
//        btAdapter = BluetoothAdapter.getDefaultAdapter();
//        BluetoothDevice device = btAdapter.getRemoteDevice(mac_address);
//        Log.d("BT_SENDING_FILE",device_name+" from client"+mac_address);
//
//        try {
//
//            // Method m = device.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
//            // btSocket = (BluetoothSocket) m.invoke(device, 1);
//
//            Method m;
//            m=device.getClass().getMethod("createRfcommSocket",new Class[] { int.class });
//            btSocket = (BluetoothSocket) m.invoke(device, Integer.valueOf(1));
//
//            if(!btSocket.isConnected()){
//                Log.d(" is connected Status",""+btSocket.isConnected());
//                //   device.createInsecureRfcommSocketToServiceRecord(UUID);
//
//            }
//            btAdapter.cancelDiscovery();
//            try{
//                btSocket.connect();
//            }catch(IOException e){
//                btSocket =(BluetoothSocket) device.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(device,1);
//                btSocket.connect();
//                Log.d("fall back","in catch clause");
//            }
//
//            byte[] msgBuffer = stringToSend.getBytes();
//            outStream = btSocket.getOutputStream();
//            outStream.write(msgBuffer);
//            if (outStream != null) {
//                outStream.flush();
//
//            }
//            result = "sent";
//            outStream.close();
//            btSocket.close();
//            Log.d("BLUETOOTH","Closing Socket");
//        } catch (Exception e) {
//            System.out.println(e);
//            Log.d("BLUETOOTH","exception while sending through bluetooth");
//            result = "failed";
//            e.getLocalizedMessage();
//        } finally{}
//        return result;
//    }

}


