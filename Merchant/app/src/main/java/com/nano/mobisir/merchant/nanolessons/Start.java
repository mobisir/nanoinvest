package com.nano.mobisir.merchant.nanolessons;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nano.mobisir.merchant.nanoMainView.MainActivity;
import com.nano.mobisir.merchant.R;
import com.nano.mobisir.merchant.nanologin.login;


public class Start extends Activity {

    private float x1,x2;
    private float y1,y2;
    LinearLayout content_layout, cover_page;
    TextView text_conent,txt_skip;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_view);
        cover_page = (LinearLayout) findViewById(R.id.cover_page);

        txt_skip = (TextView) findViewById(R.id.txt_skip);
        txt_skip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent i = new Intent(Start.this, login.class);
//                i.putExtra("lesson_id",v.getId());
//                i.putExtra("lesson", (String)v.getTag());
                startActivity(i);


            }});
    }
    protected void nextScreen(){
        content_layout = new LinearLayout(this);
        cover_page.addView(content_layout);
        LinearLayout.LayoutParams option_text_params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        option_text_params.setMargins(0, 5, 0, 0);
        // option_layout
        content_layout.setOrientation(LinearLayout.VERTICAL);
        content_layout.setGravity(Gravity.CENTER);

        text_conent = new TextView(this);
        text_conent.setGravity(Gravity.CENTER);
        // option_on_text.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_rect));
        content_layout.addView(text_conent, option_text_params);
        // option_on_text.setText(rb[i]);
        text_conent.setText("hia");
//        text_conent.setTag(rbstring[i]);
        text_conent.setTextSize(25);



    }
    public boolean onTouchEvent(MotionEvent touchevent) {
        switch(touchevent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = touchevent.getX();
                y1 = touchevent.getY();
                break;
            case MotionEvent.ACTION_UP:
                x2 = touchevent.getX();
                y2 = touchevent.getY();

                //if left to right sweep event on screen
                if (x1 < x2) {
//                        backButtonScreenSetup();
                    startActivity(new Intent(Start.this, MainActivity.class));
                }
                // if right to left sweep event on screen
                if (x1 > x2) {
//                    content_layout.removeAllViews();
//                    nextScreen();
                    startActivity(new Intent(Start.this, Start1.class));
                    // question_count=0;
                }
                // if UP to Down sweep event on screen
                // if (y1 < y2) {
                //     Toast.makeText(this, "UP to Down Swap Performed", Toast.LENGTH_LONG).show();
                // }
                // //if Down to UP sweep event on screen
                // if (y1 > y2) {
                //     Toast.makeText(this, "Down to UP Swap Performed", Toast.LENGTH_LONG).show();
                // }
                // break;
        }
        return false;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
