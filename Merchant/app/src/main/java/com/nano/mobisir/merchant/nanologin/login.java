package com.nano.mobisir.merchant.nanologin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.nano.mobisir.merchant.R;
import com.nano.mobisir.merchant.nanobiometricscanner.finger;
import com.nano.mobisir.merchant.network.Connection_detector;
import com.nano.mobisir.merchant.network.NanoUrlDetails;
import com.nano.mobisir.merchant.network.ServiceHandler;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class login extends Activity {

    protected static final String TAG = login.class.getSimpleName();
    private float x1,x2;
    private float y1,y2;
    LinearLayout content_layout, cover_page;
    TextView text_conent,text_error_msg;
    Button btn_login,btn_sign_up;
    ProgressDialog pDialog;
    EditText txt_pass;
    public static String login_id=null;
    AutoCompleteTextView txt_user;

    SharedPreferences sharedpreferences;
    org.json.JSONObject jsonObj =null;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Email = "emailKey";
    public static final String Pass = "passKey";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        cover_page = (LinearLayout) findViewById(R.id.cover_page);
        txt_user=(AutoCompleteTextView) findViewById(R.id.username);
        txt_pass=(EditText) findViewById(R.id.password);
        btn_login = (Button) findViewById(R.id.btn_sign_in);
        btn_sign_up = (Button) findViewById(R.id.btn_signup);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);


        btn_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                String userid = "";
                String password = "";
                userid = txt_user.getText().toString();
                password = txt_pass.getText().toString();

                if (userid.equals("") || password.equals("")) {

                    Toast.makeText(login.this, "Username or password must be filled", Toast.LENGTH_LONG).show();


                } else {
                    //connection detections
                    Connection_detector cd = new Connection_detector(getApplicationContext());
                    //Internet connection status
                    Boolean isInternetPresent = cd.isConnectingToInternet();

                    if (isInternetPresent) {

                        new NanoLoginFunction().execute();

                    } else {

                        Toast.makeText(login.this, "Network Not Connected", Toast.LENGTH_LONG).show();
                    }

                }


            }
        });

        btn_sign_up.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {


//                Intent i = new Intent(login.this, SignupActivity.class);
////                i.putExtra("lesson_id",v.getId());
////                i.putExtra("lesson", (String)v.getTag());
//                startActivity(i);


            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {

        }
//            Toast.makeText(getApplicationContext(), "back press",
//                    Toast.LENGTH_LONG).show();

        return false;
        // Disable back button..............
    }
    protected void nextScreen(){
        content_layout = new LinearLayout(this);
        cover_page.addView(content_layout);
        LinearLayout.LayoutParams option_text_params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        option_text_params.setMargins(0, 5, 0, 0);
        // option_layout
        content_layout.setOrientation(LinearLayout.VERTICAL);
        content_layout.setGravity(Gravity.CENTER);

        text_conent = new TextView(this);
        text_conent.setGravity(Gravity.CENTER);
        // option_on_text.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_rect));
        content_layout.addView(text_conent, option_text_params);
        // option_on_text.setText(rb[i]);
        text_conent.setText("hia");
//        text_conent.setTag(rbstring[i]);
        text_conent.setTextSize(25);



    }
    public boolean onTouchEvent(MotionEvent touchevent) {
//        switch(touchevent.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                x1 = touchevent.getX();
//                y1 = touchevent.getY();
//                break;
//            case MotionEvent.ACTION_UP:
//                x2 = touchevent.getX();
//                y2 = touchevent.getY();
//
//                //if left to right sweep event on screen
//                if (x1 < x2) {
////                        backButtonScreenSetup();
//                    startActivity(new Intent(login.this, Start3.class));
//                }
//                // if right to left sweep event on screen
//                if (x1 > x2) {
////                    content_layout.removeAllViews();
////                    nextScreen();
//                    startActivity(new Intent(login.this, finger.class));
//                    // question_count=0;
//                }
//                // if UP to Down sweep event on screen
//                // if (y1 < y2) {
//                //     Toast.makeText(this, "UP to Down Swap Performed", Toast.LENGTH_LONG).show();
//                // }
//                // //if Down to UP sweep event on screen
//                // if (y1 > y2) {
//                //     Toast.makeText(this, "Down to UP Swap Performed", Toast.LENGTH_LONG).show();
//                // }
//                // break;
//        }
        return false;
    }


    private class NanoLoginFunction extends AsyncTask<String, Void, String> {
        Boolean server_Connection_Flag=false;
        //  ProgressDialog pDialog;
        public String jsonStr=null;
        private Context context;
        String email=null;
        String pass=null;


        public NanoLoginFunction()
        {
            email=txt_user.getText().toString();
            pass = txt_pass.getText().toString();
        }
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pDialog = new ProgressDialog(login.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        //doInBackground method called to do request & get response from server
        @Override
        protected String doInBackground(String... urls) {
            final String url = "192.168.1.66:8080/nanoinvest/person";

//            loginmodel lm=new loginmodel();
//            RestTemplate restTemplate = new RestTemplate();
//            restTemplate.getMessageConverters().add(new SimpleXmlHttpMessageConverter());
//            String result = restTemplate.getForObject(url, String.class);
//            System.out.println(result);

           List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("pwd", pass));



            System.out.println("Post parameters" + params);
            ServiceHandler sh=new ServiceHandler();

            String login_url= NanoUrlDetails.nano_login_url;
            try {

                jsonStr = sh.makeServiceCall(login_url,ServiceHandler.POST,params);
//            Toast.makeText()
                //  jsonStr = sh.makeServiceCall(login_url,ServiceHandler.GET);
                System.out.println("*****************Server Response in login.java " + jsonStr);
//            Toast mymsg=Toast.makeText(login.this, "mymsg" + jsonStr, Toast.LENGTH_LONG);
//            mymsg.show();
                JSONObject jsonObj = new JSONObject(jsonStr);


                if(!jsonObj.getString("token").equals("error"))
                {
                    Log.e("Response:1", jsonObj.getString("token"));
                }
                else
                {
                    Log.e("Response:2", jsonObj.getString("error"));
                }

            } catch (Throwable e) {
            }

            return jsonStr;

//            return result;
        }

        @Override
        protected void  onPostExecute(String result){
            super.onPostExecute(result);

            //saveInDatabase();
            Toast mymsg= Toast.makeText(login.this, "mymsg" + result, Toast.LENGTH_LONG);
            mymsg.show();

            if (jsonStr.equals("Server_not_connected")) {
                text_error_msg=(TextView) findViewById(R.id.tv_error_msg);
                text_error_msg.setVisibility(TextView.VISIBLE);
                text_error_msg.setText("Server not connected !");
                server_Connection_Flag = false;
            }
            else if (jsonStr.equals("Invalid_User")) {
                System.out.println("invalied user"+jsonStr);

                text_error_msg=(TextView) findViewById(R.id.tv_error_msg);
                text_error_msg.setVisibility(TextView.VISIBLE);
                text_error_msg.setText("  Invalid User !  ");

            }
            else if (jsonStr != null) {
                System.out.println("dddddddddddddddddddddd" + jsonStr);
                login_id=jsonStr;

                try {
                    jsonObj = new JSONObject(jsonStr);
                    String merchanttoken=jsonObj.getString("token");
                    String merchantid=jsonObj.getString("username");
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("merchanttoken", merchanttoken);
                    editor.putString("merchantid", merchantid);
                    editor.commit();

                    Intent i = new Intent(login.this, finger.class);
                    startActivity(i);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            else {
                System.out.println("ServiceHandler Couldn't get any data from the url");
            }

            //dismiss process bar
            if (pDialog.isShowing())
                pDialog.dismiss();

            Log.e("server_Connection_Flag", server_Connection_Flag + "");

            if(server_Connection_Flag){




            }else{
                server_Connection_Flag = true;
                if(jsonStr.equals("Server_not_connected")){

                    //showalert

                    System.out.println("ServiceHandler Couldn't get any data from the url");
                }else if(jsonStr.equals("user_is_not_present")||jsonStr.equals("password_not_matching")){

                    // showAlertDialog

                }else{

                }

            }
        }
    }
}
