package com.nano.mobisir.merchant.nanoqrscanner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.widget.Toast;

import com.nano.mobisir.merchant.nanoMainView.Mainmenu;
import com.nano.mobisir.merchant.nanothirdparyutils.Result;
import com.nano.mobisir.merchant.nanoMainView.index;
import com.nano.mobisir.merchant.nanologin.login;
import com.nano.mobisir.merchant.nanothirdparyutils.ZBarScannerView;
import com.nano.mobisir.merchant.network.Connection_detector;
import com.nano.mobisir.merchant.network.NanoUrlDetails;
import com.nano.mobisir.merchant.network.TransactionHandler;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdul on 27/10/15.
 */


public class SimpleScannerActivity  extends Activity implements ZBarScannerView.ResultHandler {
    private ZBarScannerView mScannerView;

    ProgressDialog pDialog;


    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZBarScannerView(this);
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        Toast.makeText(this, "Contents = " + rawResult.getContents() +
                ", Format = " + rawResult.getBarcodeFormat().getName(), Toast.LENGTH_SHORT).show();
//        mScannerView.startCamera();

        String jsondata=rawResult.getContents();
      if(jsondata.equals(null)) {
               mScannerView.startCamera();

      }
        else
      {
          Intent intent = new Intent(SimpleScannerActivity.this, Mainmenu.class);
//          intent.putExtra("json_consumer", jsondata);
//          startActivity(intent);
          org.json.JSONObject jsonObj =null;


          SharedPreferences sharedpreferences = getSharedPreferences(login.MyPREFERENCES, Context.MODE_PRIVATE);
//          SharedPreferences.Editor editor = sharedpreferences.edit();
//
//          editor.clear();
//          editor.commit();
          String merchanttoken = (sharedpreferences.getString("merchanttoken", ""));
          String merchanid = (sharedpreferences.getString("merchantid", ""));
          System.out.println(merchanttoken+"bbbbbbbbbbbbbbbbbbbbb"+merchanid);


          try {
              jsonObj = new org.json.JSONObject(jsondata);
              String consumerusername=jsonObj.getString("username");
              String consumeramount=jsonObj.getString("amount");
              String consumertoken=jsonObj.getString("token");

              JSONObject obj = null;
              obj = new JSONObject();
              obj.put("merchant_token", merchanttoken);
              obj.put("consumer_username",consumerusername);
              obj.put("consumer_amount", consumeramount);
              obj.put("consumer_token", consumertoken);
              obj.put("merchanid", merchanid);

              sendUserDetailsServer(obj);


          } catch (JSONException e) {
              e.printStackTrace();
          }

  }
   }

    private void sendUserDetailsServer(JSONObject obj) {
        //connection detections
        Connection_detector cd = new Connection_detector(getApplicationContext());
        //Internet connection status
        Boolean isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            new sendUserDetailsFunction(obj).execute();

        } else {

            Toast.makeText(SimpleScannerActivity.this, "Network Not Connected", Toast.LENGTH_LONG).show();

        }


    }

    private class sendUserDetailsFunction extends AsyncTask<String, Void, String> {
        Boolean server_Connection_Flag=false;
        //  ProgressDialog pDialog;
        public String jsonStr=null;
        private Context context;
        JSONObject obj=null;
        public sendUserDetailsFunction(JSONObject obj)
        {
            this.obj=obj;
        }
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pDialog = new ProgressDialog(SimpleScannerActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        //doInBackground method called to do request & get response from server
        @Override
        protected String doInBackground(String... urls) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            try {
                params.add(new BasicNameValuePair("merchantToken", obj.getString("merchant_token")));
                params.add(new BasicNameValuePair("consumerUsername", obj.getString("consumer_username")));
                params.add(new BasicNameValuePair("consumerAmount", obj.getString("consumer_amount")));
                params.add(new BasicNameValuePair("consumerToken", obj.getString("consumer_token")));
                params.add(new BasicNameValuePair("merchantid", obj.getString("merchanid")));

                System.out.println(obj.getString("merchanid")+"aaaaaaaaaaaaaaaaaaaaaaaaa");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            TransactionHandler th=new TransactionHandler();

            String user_details_url= NanoUrlDetails.userdetail_url;
            try {

                jsonStr = th.makeServiceForUser(user_details_url, "POST", params);



            } catch (Throwable e) {
            }

            return jsonStr;

//            return result;
        }

        @Override
        protected void  onPostExecute(String result){
            super.onPostExecute(result);

            //saveInDatabase();


            if (result.equals("transaction_successfull")) {
                Toast mymsg= Toast.makeText(SimpleScannerActivity.this, "Status:" + result, Toast.LENGTH_LONG);
                mymsg.show();
                Intent i = new Intent(SimpleScannerActivity.this, index.class);
                    startActivity(i);

                server_Connection_Flag = true;
            }
            else if (result.equals("Server_not_connected")) {
                Toast msg= Toast.makeText(SimpleScannerActivity.this, "Server not connected !", Toast.LENGTH_LONG);
                msg.show();
                server_Connection_Flag = false;
            }
            else if (result.equals("transaction_failed")) {
                Toast msg= Toast.makeText(SimpleScannerActivity.this, "transaction_failed !", Toast.LENGTH_LONG);
                msg.show();

            }
//            else if (jsonStr != null) {
//
//                try {
//                    JSONObject jsonObj = new JSONObject(jsonStr);
//
//
//                    Intent i = new Intent(SimpleScannerActivity.this, finger.class);
//                    startActivity(i);
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
            else {
                System.out.println("ServiceHandler Couldn't get any data from the url");
            }


            //dismiss process bar
            if (pDialog.isShowing())
                pDialog.dismiss();

            Log.e("server_Connection_Flag", server_Connection_Flag + "");

            if(server_Connection_Flag){




            }else{
                server_Connection_Flag = true;
                if(jsonStr.equals("Server_not_connected")){

                    //showalert

                    System.out.println("ServiceHandler Couldn't get any data from the url");
                }else if(jsonStr.equals("user_is_not_present")||jsonStr.equals("password_not_matching")){

                    // showAlertDialog

                }else{

                }

            }
        }
    }

}
