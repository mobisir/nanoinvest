package com.nano.mobisir.merchant.nanothirdparyutils;

import com.nano.mobisir.merchant.nanoqrscanner.BarcodeFormat;

/**
 * Created by abdul on 27/10/15.
 */
public class Result {

    private String mContents;
    private BarcodeFormat mBarcodeFormat;

    public void setContents(String contents) {
        mContents = contents;
    }

    public void setBarcodeFormat(BarcodeFormat format) {
        mBarcodeFormat = format;
    }

    public BarcodeFormat getBarcodeFormat() {
        return mBarcodeFormat;
    }

    public String getContents() {
        return mContents;
    }
}
