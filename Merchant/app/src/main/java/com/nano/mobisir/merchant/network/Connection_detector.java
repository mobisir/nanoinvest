package com.nano.mobisir.merchant.network;

/**
 * Created by abdul on 29/10/15.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Connection_detector {

    private Context _context;

    public Connection_detector(Context context){
        this._context = context;
    }

    public boolean isConnectingToInternet(){


        System.out.println("-------------haveNetworkConnection is called-----------");
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] netInfo = connectivity.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
                    if (ni.isConnected()) {
                        haveConnectedWifi = true;
                        System.out.println("wifi");
                    }
                     }
                    if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                        if (ni.isConnected()) {
                            System.out.println("MOBILE");
                            haveConnectedMobile = true;

                        }
                    }
            }
            //  ni[i].getState() == NetworkInfo.State.CONNECTED)
            return haveConnectedWifi || haveConnectedMobile;
        }
        return false;
    }

}