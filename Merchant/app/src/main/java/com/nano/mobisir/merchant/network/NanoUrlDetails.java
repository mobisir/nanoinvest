package com.nano.mobisir.merchant.network;

/**
 * Created by abdul on 29/10/15.
 */
public class NanoUrlDetails  {

    //margent request for login url
    public final static String nano_login_url="192.168.1.82:8080/nanoinvest/loginuser";

    //margent sent request to server request url
    public final static String userdetail_url="192.168.1.82:8080/nanoinvest/investamount";

    //margent sent request to server request url
    public final static String merchantdetail_url="192.168.1.82:8080/nanoinvest/merchantnotification";

    //Samagra operator_details request url
    public final static String nano_details_url="http://54.251.169.238:8080/api/get_user_master_details/";


}