package com.nano.mobisir.merchant.network;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;


/**
 * Created by abdul on 29/10/15.
 */
public class ServiceHandler {
    static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
    private static boolean responseSuccessful = true;

    public ServiceHandler() {

    }

    public String makeServiceCall(String url, int method) {

        return this.makeServiceCall(url, method,null);
    }

    public String makeServiceCall(String url, int method,List<NameValuePair> params) {
        String jsonStr=null;
     // Checking http request method type
        if (method == POST) {

            // execute the http request and post the response
            jsonStr=getDefpost(url, "POST",params);
        }

        else if (method == GET) {

            jsonStr=getDefget(url, "GET",params);

        }

        if (responseSuccessful) {
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                if(jsonObj.isNull("token"))
                {
                    response = "Invalid_User";
                    System.out.println("*****************Server Response " + jsonStr);
                }
                else
                {
                    response = jsonStr;
                    System.out.println("*****************Server Response " + jsonStr);

                }


            }catch (Exception e)
            {

            }

        } else {


            response = "Server_not_connected";
        }



        return response;


    }

    public String makeServiceCallNotification(String url, int method,List<NameValuePair> params) {
        String jsonStr=null;
        // Checking http request method type
        if (method == POST) {

            // execute the http request and post the response
            jsonStr=getDefpost(url, "POST",params);
        }

        else if (method == GET) {

            jsonStr=getDefget(url, "GET",params);

        }

//        if (responseSuccessful) {
//            try {
//                JSONObject jsonObj = new JSONObject(jsonStr);
//                if(jsonObj.isNull("token"))
//                {
//                    response = "Invalid_User";
//                    System.out.println("*****************Server Response " + jsonStr);
//                }
//                else
//                {
//                    response = jsonStr;
//                    System.out.println("*****************Server Response " + jsonStr);
//
//                }
//
//
//            }catch (Exception e)
//            {
//
//            }
//
//        } else {
//
//
//            response = "Server_not_connected";
//        }



        return jsonStr;


    }




    public static String getDefget(String urlInput,String method,List<NameValuePair> params) {
        System.out.println("--------------"+urlInput);
        URL url = null;
        try {
            url = new URL("http://" + urlInput);
            Log.d("http", "calling " + url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            System.out.println("try get");
            HttpURLConnection conn  = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Connection", "close");
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.connect();
            InputStream in = null;
            in = (conn.getInputStream());
            String response =readIt(in) ;
            System.out.println("--------responce"+response);
//            response = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
            return response;
        } catch (IOException e) {
            Log.e("http", "Error");
            responseSuccessful = false;
            e.printStackTrace();
        }
        return "LocalClient didnt work";
    }

    public String getDefpost(String urlInput,String method,List<NameValuePair> params) {

        System.out.println("--------------"+urlInput);
        URL url = null;
        try {
            url = new URL("http://" + urlInput);
            Log.d("http", "calling " + url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            System.out.println("try post");
            HttpURLConnection conn  = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Connection", "close");
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);//milliseconds
            conn.connect();


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(params));
            writer.flush();
            writer.close();
            os.close();

            InputStream in = null;
            in = (conn.getInputStream());
            String response =readIt(in) ;
            System.out.println("--------responce"+response);
//            response = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
            return response;
        } catch (IOException e) {
            Log.e("http", "Error");
            responseSuccessful = false;
            e.printStackTrace();
        }
        return "LocalClient didnt work";


    }


    private String getQuery(List<NameValuePair
            > params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    private static String readIt(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
