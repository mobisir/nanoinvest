package com.nano.mobisir.merchant.network;

import android.util.Log;

import org.apache.http.NameValuePair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by abdul on 24/11/15.
 */
public class TransactionHandler {
    public final static int GET = 1;
    public final static int POST = 2;

    static String response = null;
    private static boolean responseSuccessful = true;
    String jsonStr;


    public TransactionHandler() {

    }


    public String makeServiceForUser(String user_details_url, String post, List<NameValuePair> params) {

        jsonStr=getNotificationForInvestment(user_details_url, "POST",params);
        System.out.println("nnnnnnnnnnnnnn"+jsonStr);
        return jsonStr;
    }

    private String getNotificationForInvestment(String user_details_url, String post, List<NameValuePair> params) {

        System.out.println("--------------"+user_details_url);
        URL url = null;
        try {
            url = new URL("http://" + user_details_url);
            Log.d("http", "calling " + url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            System.out.println("try post");
            HttpURLConnection conn  = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Connection", "close");
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);//milliseconds
            conn.connect();


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(params));
            writer.flush();
            writer.close();
            os.close();

            InputStream in = null;
            in = (conn.getInputStream());
            String response =readIt(in) ;
            System.out.println("--------responce"+response);
//            response = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
            return response;
        } catch (IOException e) {
            Log.e("http", "Error");
            responseSuccessful = false;
            e.printStackTrace();
        }

        return "LocalClient didnt work";
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private static String readIt(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
