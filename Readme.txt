First readme file

Technologies used In NanoInvest server
Consumer Application

Android:
Os version: Ubuntu 14.04
IDE :Android Studio
Java 1.8
android SDK :23 

IOs:
OS version:OS X mervericks10.10.4
Xcode 7.0.1
IOS SDK 9.0
Deployment target min version 7.1 to latest


NanoServer

Spring tool suit 3.7.1.RELEASE
apche-Maven 3.3
java 1.7 (we can use 1.8 too) 
MySql 5.6
Spring franework 4.2.2.RELESE
apache tomcat 7.0

steps to run server application in command line with maven dependencies in mac:

step1:download maven
http://maven.apache.org/download.cgi

step2:Add environment path variable in .bash_profile for mac and in .bashrc for linux machine.follow the below steps.

open .bash_profile -> vi .bash_profile
add below lines
export M2_HOME=<your path to apache maven>/apache-maven-3.3.3
export PATH=$PATH:$M2_HOME/bin
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_79.jdk/Contents/Home
run command -> source .bash_profile

step3:check maven version -> mvn -version
step4:change your path to project directory.

change your directory path to project directory : cd nanoinvest/server/nanoInvestServer

step 5: mvn clean
step 6: mvn package
step 7: move .war file to your tomcat’s webapps/ROOT directory
step 8: extract it and keep only WEB-INF and META-INF 
step 9: start the tomcat server
step 10: run in your local machine.

Install maven dependencies in Linux:

step 1: apt-cache search maven
step 2: sudo apt-get install maven
step 3: set environment path variable
step 4: mvn -version





