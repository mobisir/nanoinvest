package com.mobisir.nano.nano;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import java.io.File;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
/**
 * Created by abdul on 21/10/15.
 */
public class BankInfoActivity extends Activity{

    TextView mynanoinvest,settings,notifications,profile,bankinfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.bankinfo);

        mynanoinvest = (TextView) findViewById(R.id.txt_mynanoinvest);
        settings = (TextView) findViewById(R.id.txt_settings);
        notifications = (TextView) findViewById(R.id.txt_notifications);
        profile = (TextView) findViewById(R.id.txt_profile);
        bankinfo = (TextView) findViewById(R.id.txt_bankinfo);

        mynanoinvest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

//                try {
//                    Intent intent = new Intent(BankInfoActivity.this, myNanoInvestInfo.class);
//                    startActivity(intent);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(BankInfoActivity.this, ProfileActivity.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(BankInfoActivity.this, NotificationActivity.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(BankInfoActivity.this, ProfileActivity.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        bankinfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(BankInfoActivity.this, BankInfoActivity.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });



    }


}
