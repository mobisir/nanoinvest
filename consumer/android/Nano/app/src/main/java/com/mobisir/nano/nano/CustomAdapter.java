package com.mobisir.nano.nano;

/**
 * Created by abdul on 20/10/15.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
public class CustomAdapter extends BaseAdapter{

    Context context;
    String [] dates;
    String [] amount;
    String [] npv;
    int [] status;
    int [] detail;
    private static LayoutInflater inflater=null;


    public CustomAdapter(mynanoinvest NanoInvestInfo, String[] prodates,String[] proamount,String[] pronpv,int[] prostatus, int[] prodetail) {
        context=NanoInvestInfo;

        dates=prodates;
        amount=proamount;
        npv=pronpv;
        status=prostatus;
        detail=prodetail;

        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);



    }

    public class Holder
    {
        TextView dt;
        TextView am;
        TextView np;
        ImageView st;
        ImageView de;
    }

    @Override
    public int getCount() {
        return dates.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.nanoinvestinfolist, null);
        holder.dt=(TextView) rowView.findViewById(R.id.textViewdates);
        holder.am=(TextView) rowView.findViewById(R.id.textViewamount);
        holder.np=(TextView) rowView.findViewById(R.id.textViewnpv);
        holder.st=(ImageView) rowView.findViewById(R.id.imageViewstatus);
        holder.de=(ImageView) rowView.findViewById(R.id.imageViewdetail);


        holder.dt.setText(dates[position]);
        holder.am.setText(amount[position]);
        holder.np.setText(npv[position]);
        holder.st.setImageResource(status[position]);
        holder.de.setImageResource(detail[position]);


        rowView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked "+amount[position], Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }

}
