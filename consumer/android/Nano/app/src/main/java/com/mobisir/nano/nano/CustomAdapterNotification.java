
package com.mobisir.nano.nano;

/**
 * Created by abdul on 20/10/15.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
public class CustomAdapterNotification extends BaseAdapter {

    Context context;
    String [] No;
    String [] Description;
    String [] Dateandtime;

    private static LayoutInflater inflater=null;


    public CustomAdapterNotification(NotificationActivity notificationactivity, String[] prono,String[] prodescription,String[] predateandtime) {
        context=notificationactivity;

        No=prono;
        Description=prodescription;
        Dateandtime=predateandtime;

        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);



    }

    public class Holder
    {
        TextView slno;
        TextView desc;
        TextView dt;

    }

    @Override
    public int getCount() {
        return No.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.notification_list, null);
        holder.slno=(TextView) rowView.findViewById(R.id.textViewNo);
        holder.desc=(TextView) rowView.findViewById(R.id.textViewDescription);
        holder.dt=(TextView) rowView.findViewById(R.id.textViewDateAndTime);



        holder.slno.setText(No[position]);
        holder.desc.setText(Description[position]);
        holder.dt.setText(Dateandtime[position]);


        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked " + No[position], Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }

}
