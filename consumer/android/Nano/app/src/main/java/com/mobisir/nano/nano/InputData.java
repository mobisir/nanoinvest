package com.mobisir.nano.nano;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mobisir.nano.nano.network.Connection_detector;
import com.mobisir.nano.nano.network.NanoUrlDetails;
import com.mobisir.nano.nano.network.ServiceHandler;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdul on 27/11/15.
 */
public class InputData  extends Activity{

       EditText amount;
       Button bt_continue;
        ProgressDialog pDialog;
    JSONObject obj=null;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.input_data);

            amount = (EditText) findViewById(R.id.txt_amount);
            bt_continue=(Button) findViewById(R.id.btn_continue);

            bt_continue.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    if(amount.getText().length()>0)
                    {


                        SharedPreferences sharedpreferences = getSharedPreferences(login.MyPREFERENCES, Context.MODE_PRIVATE);
//
                        String consumertoken = (sharedpreferences.getString("consumertoken", ""));
                        String consumerid = (sharedpreferences.getString("consumerid", ""));
                       try{
                        JSONObject obj = null;
                        obj = new JSONObject();
                        obj.put("username", consumerid);
                        obj.put("amount",amount.getText().toString());
                        obj.put("token", consumertoken);


                Intent i = new Intent(InputData.this, index.class);
                i.putExtra("amountInput", obj.toString());
                startActivity(i);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

//                        System.out.println(consumertoken+"bbbbbbbbbbbbbbbbbbbbb"+consumerid);
//
//
//
//                        Connection_detector cd = new Connection_detector(getApplicationContext());
//                        //Internet connection status
//                        Boolean isInternetPresent = cd.isConnectingToInternet();
//
//                        if (isInternetPresent) {
//
//                            new sendRequestForToken(amount.getText().toString(),consumertoken,consumerid).execute();
//
//                        } else {
//
//                            Toast.makeText(InputData.this, "Network Not Connected", Toast.LENGTH_LONG).show();
//
//                        }


                    }
                    else
                    {
                        Toast mymsg=Toast.makeText(InputData.this, "Enter Amount" , Toast.LENGTH_LONG);
                        mymsg.show();
//                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(InputData.this);
//                        alertDialogBuilder.show();
                    }



                }
            });
        }

//    private class sendRequestForToken extends AsyncTask<String, Void, String> {
//        Boolean server_Connection_Flag=false;
//        //  ProgressDialog pDialog;
//        public String jsonStr=null;
//        private Context context;
//        String amount=null;
//        String consumertoken=null;
//        String cosumerid=null;
//
//
//        public sendRequestForToken(String amount,String consumertoken,String cosumerid)
//        {
//            this.amount=amount;
//            this.consumertoken=consumertoken;
//            this.cosumerid=cosumerid;
//        }
//        @Override
//        protected void onPreExecute() {
//
//            super.onPreExecute();
//            pDialog = new ProgressDialog(InputData.this);
//            pDialog.setMessage("Loading...");
//            pDialog.setCancelable(false);
//            pDialog.show();
//
//        }
//
//        //doInBackground method called to do request & get response from server
//        @Override
//        protected String doInBackground(String... urls) {
//
//            List<NameValuePair> params = new ArrayList<NameValuePair>();
//
//
//                params.add(new BasicNameValuePair("consumerUsername", cosumerid));
//                params.add(new BasicNameValuePair("consumerAmount", amount));
//                params.add(new BasicNameValuePair("consumerToken", consumertoken));
//
//
//            ServiceHandler th=new ServiceHandler();
//
//            String user_details_url= NanoUrlDetails.gettoken_url;
//            try {
//
//                jsonStr = th.makeServiceCall(user_details_url, ServiceHandler.POST, params);
//
//
//
//            } catch (Throwable e) {
//            }
//
//            return jsonStr;
//
////            return result;
//        }
//
//        @Override
//        protected void  onPostExecute(String result){
//            super.onPostExecute(result);
//
//            //saveInDatabase();
//
//
//            if (result.equals("transaction_successfull")) {
//                Toast mymsg= Toast.makeText(InputData.this, "Status:" + result, Toast.LENGTH_LONG);
//                mymsg.show();

//
//                server_Connection_Flag = true;
//            }
//            else if (result.equals("Server_not_connected")) {
//                Toast msg= Toast.makeText(InputData.this, "Server not connected !", Toast.LENGTH_LONG);
//                msg.show();
//                server_Connection_Flag = false;
//            }
//            else if (result.equals("transaction_failed")) {
//                Toast msg= Toast.makeText(InputData.this, "transaction_failed !", Toast.LENGTH_LONG);
//                msg.show();
//
//            }
//
//            else {
//                System.out.println("ServiceHandler Couldn't get any data from the url");
//            }
//
//
//            //dismiss process bar
//            if (pDialog.isShowing())
//                pDialog.dismiss();
//
//            Log.e("server_Connection_Flag", server_Connection_Flag + "");
//
//            if(server_Connection_Flag){
//
//
//
//
//            }else{
//                server_Connection_Flag = true;
//                if(jsonStr.equals("Server_not_connected")){
//
//
//                    System.out.println("ServiceHandler Couldn't get any data from the url");
//                }else if(jsonStr.equals("user_is_not_present")||jsonStr.equals("password_not_matching")){
//
//                    // showAlertDialog
//
//                }else{
//
//                }
//
//            }
//        }


//    }








}
