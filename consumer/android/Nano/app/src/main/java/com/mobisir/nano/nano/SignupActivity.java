package com.mobisir.nano.nano;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.mobisir.nano.nano.network.Connection_detector;
import com.mobisir.nano.nano.network.NanoUrlDetails;
import com.mobisir.nano.nano.network.ServiceHandler;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdul on 29/10/15.
 */
public class SignupActivity extends Activity {
    Button btn_register;
    TextView emailid,username,password;
    ProgressDialog mProgressDialog;
    ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        btn_register = (Button) findViewById(R.id.btn_signup);
        emailid = (TextView) findViewById(R.id.txt_emailid);
        username = (TextView) findViewById(R.id.txt_username);
        password = (TextView) findViewById(R.id.txt_password);
        btn_register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                   registration();

            }
        });
    }



    private void registration() {

        String email=emailid.getText().toString();

        String user = username.getText().toString();

        String pass = password.getText().toString();

        if(username.equals("") || password.equals("") || emailid.equals("")){

            Toast.makeText(SignupActivity.this, "Username or password or email must be filled", Toast.LENGTH_LONG).show();

            return;

        }else
        {
                //connection detections
                Connection_detector cd = new Connection_detector(getApplicationContext());
                //Internet connection status
                Boolean isInternetPresent = cd.isConnectingToInternet();

                if(isInternetPresent)
                {

                    new NanoLoginFunction(this).execute();

                }

        }
    }

    private class NanoLoginFunction extends AsyncTask<String, Void, String> {
        Boolean server_Connection_Flag=false;
      //  ProgressDialog pDialog;
        String jsonStr=null;


        private Context context;

        public NanoLoginFunction(Context context)
        {
//            this.context = context;
//            mProgressDialog = new ProgressDialog(context);
//            mProgressDialog.setMessage("receving file..");
//            mProgressDialog.setIndeterminate(false);
//            mProgressDialog.setMax(100);
//            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//            mProgressDialog.setCancelable(true);


        }
        //onPreExecute method  called before request to server

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pDialog = new ProgressDialog(SignupActivity.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();

        }


        //doInBackground method called to do request & get response from server


        @Override
        protected String doInBackground(String... urls){



            ContentValues values = new ContentValues();
            values.put("key1", "value1");
            values.put("key2", 123);
            //add network parameter to url
            List<ContentValues> params = new ArrayList<ContentValues>();

            params.add(values);

           ServiceHandler sh=new ServiceHandler();

            String login_url= NanoUrlDetails.nano_login_url;
            try {

                 jsonStr = sh.makeServiceCall(login_url,ServiceHandler.GET);


                Log.e("Response:",jsonStr);

            } catch (Throwable e) {


            }if (jsonStr.equals("Server_not_connected")) {

                server_Connection_Flag = false;


            }
            else if (jsonStr != null) {
                try {

                    JSONObject obj = new JSONObject(jsonStr);

                    //represent that operator is active or inactive
                    String jusername = obj.getString("username");

                    //operator id
                    String jpassword = obj.getString("password");
                    System.out.println(jusername+jpassword);

                }catch(JSONException e) {
                    e.printStackTrace();

                }
            }
            else {
                System.out.println("ServiceHandler Couldn't get any data from the url");
            }


            return null;

        }

        @Override
        protected void  onPostExecute(String result){
            super.onPostExecute(result);

            //dismiss process bar
            if (pDialog.isShowing())
                pDialog.dismiss();

            Log.e("server_Connection_Flag",server_Connection_Flag+"");

            if(server_Connection_Flag){

                //saveInDatabase();


            }else{
                server_Connection_Flag = true;
                if(jsonStr.equals("Server_not_connected")){

                    //showalert

                    System.out.println("ServiceHandler Couldn't get any data from the url");
                }else if(jsonStr.equals("user_is_not_present")||jsonStr.equals("password_not_matching")){

                    // showAlertDialog

                }else{

                }

            }
        }
    }
}

