package com.mobisir.nano.nano;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.zxing.WriterException;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
public class index extends Activity {

    private float x1, x2;
    private float y1, y2;
    LinearLayout content_layout, cover_page;
    TextView text_conent,text_mainmenu,text_signup;
    Context context = this;
    ImageView bluetooth, barcode, bumb;
    ImageView btn;
    BluetoothAdapter bluetooth1;
    protected static final String TAG = "BLUETOOTH";
    private BluetoothAdapter BA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.index);
        cover_page = (LinearLayout) findViewById(R.id.cover_page);
        bluetooth = (ImageView) findViewById(R.id.btn_bluetooth1);
        barcode = (ImageView) findViewById(R.id.btn_barcode1);
        bumb = (ImageView) findViewById(R.id.btn_bumb1);
        text_mainmenu=(TextView) findViewById(R.id.txtmainmenu);
        text_signup=(TextView) findViewById(R.id.txtlogoff);
        Intent intent = getIntent();
        final String qrInputText = intent.getStringExtra("amountInput");
        System.out.println("input data = "+qrInputText);

        bluetooth.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {









                //original


//                BluetoothAdapter btadapter = BluetoothAdapter.getDefaultAdapter();
////                BluetoothDevice btdev = btadapter.getRemoteDevice(address);
//                Uri uri = Uri.fromFile(new File("/storage/emulated/0/DCIM/Camera/sss.jpg"));
//
//                Intent shareIntent = new Intent();
//                shareIntent.setAction(Intent.ACTION_SEND);
//                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, qrInputText);
//                System.out.println(uri);
//                shareIntent.setType("text/plain");
//                shareIntent.setComponent(new ComponentName(
//                        "com.android.bluetooth",
//                        "com.android.bluetooth.opp.BluetoothOppLauncherActivity"));
//
//                startActivity(shareIntent);


            }
        });



        barcode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


//                    Button btnDismiss = (Button)popupView.findViewById(R.id.dismiss);
//                    btnDismiss.setOnClickListener(new Button.OnClickListener() {
//
//                        @Override
//                        public void onClick(View v) {
//                            // TODO Auto-generated method stub
//                            popupWindow.dismiss();
//                        }
//                    });
//


//                Find screen size
                WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
                Display display = manager.getDefaultDisplay();
                Point point = new Point();
                display.getSize(point);
                int width = point.x;
                int height = point.y;
                int smallerDimension = width < height ? width : height;
                smallerDimension = smallerDimension * 3 / 4;
                System.out.println("screen size"+smallerDimension);

//                Encode with a QR Code image
                QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                        null,
                        Contents.Type.TEXT,
                        com.google.zxing.BarcodeFormat.QR_CODE.toString(),
                        smallerDimension);

                try {
                    Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();

                    AlertDialog.Builder imageDialog = new AlertDialog.Builder(context);
                    LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View layout = inflater.inflate(R.layout.popup, null);
                    ImageView image = (ImageView) layout.findViewById(R.id.Qr_output);
                    image.setImageBitmap(bitmap);
                    imageDialog.setView(layout);
                    imageDialog.create();
                    imageDialog.show();


                } catch (WriterException e) {
                    e.printStackTrace();
                }

            }
        });

        bumb.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(index.this, Mainmenu.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        });


        text_mainmenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(index.this, Mainmenu.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

//                try {
//                    Intent intent = new Intent(index.this, myNanoInvestInfo.class);
//                    startActivity(intent);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }


        });

        text_signup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences sharedpreferences = getSharedPreferences(login.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();

//                try {
//                    Intent intent = new Intent(index.this, AndroidScanner.class);
//                    startActivity(intent);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }


            }


        });
    }



    //    protected void nextScreen(){
//        content_layout = new LinearLayout(this);
//        cover_page.addView(content_layout);
//        LinearLayout.LayoutParams option_text_params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        option_text_params.setMargins(0, 5, 0, 0);
//        // option_layout
//        content_layout.setOrientation(LinearLayout.VERTICAL);
//        content_layout.setGravity(Gravity.CENTER);
//
//        text_conent = new TextView(this);
//        text_conent.setGravity(Gravity.CENTER);
//        // option_on_text.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_rect));
//        content_layout.addView(text_conent, option_text_params);
//        // option_on_text.setText(rb[i]);
//        text_conent.setText("hia");
////        text_conent.setTag(rbstring[i]);
//        text_conent.setTextSize(25);
//
//
//
//    }
    public boolean onTouchEvent(MotionEvent touchevent) {

//        switch (touchevent.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                x1 = touchevent.getX();
//                y1 = touchevent.getY();
//                break;
//            case MotionEvent.ACTION_UP:
//                x2 = touchevent.getX();
//                y2 = touchevent.getY();
//
//                //if left to right sweep event on screen
//                if (x1 < x2) {
//                    startActivity(new Intent(index.this, finger.class));
////                        backButtonScreenSetup();
//                }
//                // if right to left sweep event on screen
//                if (x1 > x2) {
////                    content_layout.removeAllViews();
////                    nextScreen();
//                    startActivity(new Intent(index.this, MainActivity.class));
//                    // question_count=0;
//                }
//                // if UP to Down sweep event on screen
//                // if (y1 < y2) {
//                //     Toast.makeText(this, "UP to Down Swap Performed", Toast.LENGTH_LONG).show();
//                // }
//                // //if Down to UP sweep event on screen
//                // if (y1 > y2) {
//                //     Toast.makeText(this, "Down to UP Swap Performed", Toast.LENGTH_LONG).show();
//                // }
//                // break;
//        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
            }
//            if(resultCode == RESULT_CANCELLED){
//                //handle cancel
//            }
        }
    }












    private BluetoothSocket transferSocket;

    private void connectToServerSocket(BluetoothDevice device, UUID uuid) {
        try{
            BluetoothSocket clientSocket
                    = device.createRfcommSocketToServiceRecord(uuid);

            // Block until server connection accepted.
            clientSocket.connect();

            // Start listening for messages.
            StringBuilder incoming = new StringBuilder();
            listenForMessages(clientSocket, incoming);

            // Add a reference to the socket used to send messages.
            transferSocket = clientSocket;

        } catch (IOException e) {
            Log.e("BLUETOOTH", "Blueooth client I/O Exception", e);
        }
    }



    private boolean listening = false;

    private void listenForMessages(BluetoothSocket socket,
                                   StringBuilder incoming) {
        listening = true;


        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        try {
            InputStream instream = socket.getInputStream();
            int bytesRead = -1;

            while (listening) {
                bytesRead = instream.read(buffer);
                if (bytesRead != -1) {
                    String result = "";
                    while ((bytesRead == bufferSize) &&
                            (buffer[bufferSize-1] != 0)){
                        result = result + new String(buffer, 0, bytesRead - 1);
                        bytesRead = instream.read(buffer);
                    }
                    result = result + new String(buffer, 0, bytesRead - 1);
                    incoming.append(result);
                }
                socket.close();
            }
        } catch (IOException e) {
            Log.e(TAG, "Message received failed.", e);
        }
        finally {
        }
    }





}