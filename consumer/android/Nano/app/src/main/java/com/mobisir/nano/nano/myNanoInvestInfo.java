package com.mobisir.nano.nano;

import android.app.Activity;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobisir.nano.nano.network.Connection_detector;
import com.mobisir.nano.nano.network.NanoUrlDetails;
import com.mobisir.nano.nano.network.ServiceHandler;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

/**
 * Created by abdul on 20/10/15.
 */
public class myNanoInvestInfo extends Activity {
    ListView lv;
    Context context;
    ProgressDialog pDialog;
    TextView mynanoinvest,settings,notifications;

    ArrayList prgmName;
    public static String [] dates={"oct-12-2015","oct-12-2015"};
    public static String [] amount={"30","50"};
    public static String [] npv={"200.0","235"};
    public static int [] status={R.drawable.down,R.drawable.down};
    public static int [] detail={R.drawable.bar,R.drawable.bb};




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mynanoinverstinfo);
//        mynanoinvest = (TextView) findViewById(R.id.txt_mynanoinvest);
//        settings = (TextView) findViewById(R.id.txt_settings);
//        notifications = (TextView) findViewById(R.id.txt_notifications);
//        context=this;
//
//
//        getNotificationFromServer();
//
//
//        lv=(ListView) findViewById(R.id.listView);
//        lv.setAdapter(new CustomAdapter(this, dates,amount,npv,status,detail));



        mynanoinvest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(myNanoInvestInfo.this, myNanoInvestInfo.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(myNanoInvestInfo.this, ProfileActivity.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(myNanoInvestInfo.this, NotificationActivity.class);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    private void getNotificationFromServer() {
 //connection detections
        Connection_detector cd = new Connection_detector(getApplicationContext());
        //Internet connection status
        Boolean isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            new PopulateNotification().execute();

        } else {

            Toast.makeText(myNanoInvestInfo.this, "Network Not Connected", Toast.LENGTH_LONG).show();
        }


    }





    private class PopulateNotification extends AsyncTask<String, Void, String> {
        Boolean server_Connection_Flag=false;
        //  ProgressDialog pDialog;
        public String jsonStr=null;
        private Context context;
        String email=null;
        String pass=null;


        public PopulateNotification()
        {

        }
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pDialog = new ProgressDialog(myNanoInvestInfo.this);
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        //doInBackground method called to do request & get response from server
        @Override
        protected String doInBackground(String... urls) {

            SharedPreferences sharedpreferences = getSharedPreferences(login.MyPREFERENCES, Context.MODE_PRIVATE);
            String consumertoken = (sharedpreferences.getString("consumertoken", ""));
            String consumerid = (sharedpreferences.getString("consumerid", ""));
            System.out.println(consumertoken + "bbbbbbbbbbbbbbbbbbbbb" + consumerid);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("consumerUsername", consumerid));
            params.add(new BasicNameValuePair("consumerToken", consumertoken));


            ServiceHandler sh=new ServiceHandler();

            String login_url= NanoUrlDetails.notifications_for_consumer_url;
            try {

                jsonStr = sh.makeServiceCall(login_url,ServiceHandler.POST,params);
//
                JSONObject jsonObj = new JSONObject(jsonStr);


                if(!jsonObj.getString("token").equals("error"))
                {
                    Log.e("Response:1", jsonObj.getString("token"));
                }
                else
                {
                    Log.e("Response:2", jsonObj.getString("error"));
                }

            } catch (Throwable e) {
            }

            return jsonStr;

//            return result;
        }

        @Override
        protected void  onPostExecute(String result){
            super.onPostExecute(result);

            System.out.println("zzzzzzzzzzzzzzzzzzzzzz"+result);
            //saveInDatabase();
            Toast mymsg=Toast.makeText(myNanoInvestInfo.this, "Transaction details" + result, Toast.LENGTH_LONG);
            mymsg.show();


            if (jsonStr.equals("Server_not_connected")) {

                server_Connection_Flag = false;
            }
            else if (jsonStr.equals("Invalid_User")) {
                System.out.println("invalied user" + jsonStr);

            }
            else if (jsonStr != null) {



//                Intent i = new Intent(myNanoInvestInfo.this, NotificationActivity.class);
////                i.putExtra("loginkey_id",jsonStr);
//                startActivity(i);

            }
            else {
                System.out.println("ServiceHandler Couldn't get any data from the url");
            }

            //dismiss process bar
            if (pDialog.isShowing())
                pDialog.dismiss();

            Log.e("server_Connection_Flag",server_Connection_Flag+"");

            if(server_Connection_Flag){




            }else{
                server_Connection_Flag = true;
                if(jsonStr.equals("Server_not_connected")){

                    //showalert

                    System.out.println("ServiceHandler Couldn't get any data from the url");
                }else if(jsonStr.equals("user_is_not_present")||jsonStr.equals("password_not_matching")){

                    // showAlertDialog

                }else{

                }

            }
        }
    }

}
