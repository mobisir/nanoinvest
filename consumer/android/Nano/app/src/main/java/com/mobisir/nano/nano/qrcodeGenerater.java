package com.mobisir.nano.nano;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

/**
 * Created by abdul on 15/10/15.
 */
public class qrcodeGenerater extends Activity {

    private String LOG_TAG = "GenerateQRCode";
    Button barcode_button;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcodegeneraterview);

        barcode_button = (Button) findViewById(R.id.qr_button);
        barcode_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                EditText qrInput = (EditText) findViewById(R.id.qrInput);
                String qrInputText = qrInput.getText().toString();
                Log.v(LOG_TAG, qrInputText);

                //Find screen size
                WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
                Display display = manager.getDefaultDisplay();
                Point point = new Point();
                display.getSize(point);
                int width = point.x;
                int height = point.y;
                int smallerDimension = width < height ? width : height;
                smallerDimension = smallerDimension * 3 / 4;

                //Encode with a QR Code image
                QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                        null,
                        Contents.Type.TEXT,
                        BarcodeFormat.QR_CODE.toString(),
                        smallerDimension);
                try {
                    Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
                    ImageView myImage = (ImageView) findViewById(R.id.img_bar);
                    myImage.setImageBitmap(bitmap);

                } catch (WriterException e) {
                    e.printStackTrace();
                }


                // More buttons go here (if any) ...

            }


//                QRCodeWriter writer = new QRCodeWriter();
//                try {
//                    BitMatrix bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 512, 512);
//                    int width = bitMatrix.getWidth();
//                    int height = bitMatrix.getHeight();
//                    Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
//                    for (int x = 0; x < width; x++) {
//                        for (int y = 0; y < height; y++) {
//                            bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
//                        }
//                    }
//                    ((ImageView) findViewById(R.id.img_result_qr)).setImageBitmap(bmp);
//
//                } catch (WriterException e) {
//                    e.printStackTrace();
//                }


        });

    }
    }

