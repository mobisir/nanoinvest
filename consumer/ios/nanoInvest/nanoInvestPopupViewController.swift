//
//  nanoInvestPopupViewController.swift
//  nanoInvest
//
    

import Foundation
import UIKit
import QuartzCore
//@objc(nanoInvestPopupViewController)

class nanoInvestPopupViewController : UIViewController {
    
    
    
    @IBAction func popupclose(sender: AnyObject) {
        
//        dismissViewControllerAnimated(true,
//            completion: nil)
   
   self.removeAnimate()
        
    }
    
    @IBOutlet weak var qrmsglabel: UILabel!
    @IBOutlet weak var nanoInvestQrimg: UIImageView!
    
  
    @IBOutlet var popupView: UIView!
   
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        self.popupView.layer.cornerRadius = 5
        self.popupView.layer.shadowOpacity = 0.8
        self.popupView.layer.shadowOffset = CGSizeMake(0.0, 0.0)
    }
    
    func showInView(aView: UIView!, withImage image : UIImage!, withMessage message: String!, animated: Bool)
    {
        aView.addSubview(self.view)
        print("Hello new Print with in pop up stringnew line",image);

        nanoInvestQrimg!.image = image
        qrmsglabel!.text = message
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
}

    
}

