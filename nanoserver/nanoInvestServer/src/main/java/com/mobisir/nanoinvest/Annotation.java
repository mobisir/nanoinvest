package com.mobisir.nanoinvest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import org.springframework.scheduling.annotation.Scheduled;

import org.springframework.stereotype.Service;

import com.mobisir.nanoinvest.dao.UserDao;
import com.mobisir.nanoinvest.model.AuthenticationResultBean;
import com.mobisir.nanoinvest.service.GeneralUserService;

import java.util.Date;

/*
 * start script for delete session
 */


@Service
@EnableScheduling
public class Annotation {
	@Autowired
	private GeneralUserService generalUserService;

	public void setGeneralUserService(GeneralUserService generalUserService) {
		this.generalUserService = generalUserService;
	}

	@Scheduled(fixedDelay = 86400)
	public void fixedDelayTask() {
		System.out.println(new Date() + " This runs in a fixed delay");

		generalUserService.deleteSession();

	}
}
