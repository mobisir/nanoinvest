package com.mobisir.nanoinvest;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mobisir.nanoinvest.model.AdminModel;
import com.mobisir.nanoinvest.model.AuthenticationResultBean;
import com.mobisir.nanoinvest.model.User;
import com.mobisir.nanoinvest.model.sessionModel;
import com.mobisir.nanoinvest.model.tokenmodel;
import com.mobisir.nanoinvest.service.GeneralUserService;
import com.mobisir.nanoinvest.service.TransactionService;
import com.mobisir.nanoinvest.service.adminService;
import com.mobisir.nanoinvest.utils.GenerateNanoToken;
import com.mobisir.nanoinvest.utils.NanoConstants;
import com.mobisir.nanoinvest.validator.LoginFormValidator;
import com.mobisir.nanoinvest.validator.UserFormValidator;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private adminService adminService;

	public void setUserService(adminService adminService) {
		this.adminService = adminService;
	}

	@Autowired
	private GeneralUserService generalUserService;

	public void setGeneralUserService(GeneralUserService generalUserService) {
		this.generalUserService = generalUserService;
	}
	
	@Autowired
	private TransactionService trnService;

	public void setAdminService(adminService adminService) {
		this.adminService = adminService;
	}

	public void setTrnService(TransactionService trnService) {
		this.trnService = trnService;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

		logger.info("Welcome home! The client locale is {}.", locale);
		return "redirect:/users";

	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String showlogin(Model model) {

		logger.debug("showlogin()");
		logger.info("Welcome home! The client locale is {}.", "in user");
		model.addAttribute("users", "");
		return "users/login";

	}

	// change user to use
	@RequestMapping(value = "users/user", method = RequestMethod.POST)
	public String login(@RequestParam("email") String eaddress, @RequestParam("pwd") String pword,
			@RequestHeader("User-Agent") String userAgent, @ModelAttribute("clientIpAddress") String clientIpAddress,
			HttpSession session, @ModelAttribute("loginusr") User usr, BindingResult bindingResult, Model model) {

		LoginFormValidator validatelogin = new LoginFormValidator();

		validatelogin.validate(usr, bindingResult);

		if (bindingResult.getErrorCount() > 0) {

			// show error values on jsp screen

			return "users/login";

		}

		logger.debug("login()");

		JSONObject obj = null;
		AdminModel userDetails = new AdminModel();
		sessionModel sessionmodel = new sessionModel();
		userDetails.setEmail(eaddress);
		userDetails.setPwd(pword);
		UUID idOne = UUID.randomUUID();
		sessionmodel.setSession_id(idOne.toString());
		sessionmodel.setIp_address(clientIpAddress);
		sessionmodel.setUser_id(eaddress);
		sessionmodel.setUser_agent(userAgent);
		sessionmodel.setSession_data(eaddress);
		session.setAttribute("session_id", idOne);
		session.setAttribute("email_id", eaddress);
		session.setMaxInactiveInterval(20 * 60);

		AuthenticationResultBean res = generalUserService.AuthenticateUser(userDetails);

		if (res.isLoginSuccess()) {

			generalUserService.saveSession(sessionmodel);

			obj = new JSONObject();
			String token = GenerateNanoToken.getRandomToken();
			obj.put("token", token);

			if (res.getRole() != null && res.getRole().trim().length() > 0) {

				if (res.getRole().equals(NanoConstants.ADMIN)) {

					return "redirect:/admin";
				} else if (res.getRole().equals(NanoConstants.MERCHANT)) {

					return "users/user";

				} else if (res.getRole().equals(NanoConstants.CONSUMER)) {

					return "users/error";
				}
			}

			return "users/error";

		} else {

			return "users/error";

		}
	}

	@RequestMapping(value = "users/user", method = RequestMethod.POST, params = "signup")
	public String signup(Locale locale, Model model) {
		logger.debug("signup()");
		return "users/signup";
	}

	@RequestMapping(value = "users/signup", method = RequestMethod.POST)
	public String getMethod(@RequestParam("firstName") String fname, @RequestParam("lastName") String lName,
			@RequestParam("emailAddress") String eaddress, @RequestParam("passWord") String pWord,
			@RequestParam("ph") String ph, @RequestParam("role") String role, @RequestParam("dob") String dob,
			@RequestParam("addr") String addr, @RequestParam("city") String city, @RequestParam("state") String state,
			@RequestParam("qus") String ques, @RequestParam("ans") String ans,
			@RequestParam("bankname") String bankname, @RequestParam("bankac") String bankac,
			@RequestParam("bankbr") String bankbr, @RequestParam("bankifsc") String bankifsc,
			@RequestParam("bankaddr") String bankaddr, @RequestParam("bankcity") String bankcity,
			@RequestParam("bankstate") String bankstate) {

		logger.debug("getMethod()");
		AdminModel sm = new AdminModel();
		sm.setFname(fname);
		sm.setLname(lName);
		sm.setEmail(eaddress);
		sm.setPwd(pWord);
		sm.setPh(ph);
		sm.setRole(role);
		sm.setDob(dob);
		sm.setAddress(addr);
		sm.setCity(city);
		sm.setState(state);
		sm.setQues(ques);
		sm.setAns(ans);

		sm.setBankname(bankname);
		sm.setBankac(bankac);
		sm.setBankbr(bankbr);
		sm.setBankifsc(bankifsc);
		sm.setBankaddr(bankaddr);
		sm.setBankcity(bankcity);
		sm.setBankstate(bankstate);

		generalUserService.save(sm);

		return "users/login";
	}

	@RequestMapping(value = "/loginuser", method = RequestMethod.POST)
	public @ResponseBody String getLoginJSON(Model model, @RequestParam("email") String username,
			@RequestParam("pwd") String password, HttpSession session) {

		logger.debug("getAllEmployeesJSON()");
		JSONObject obj = null;
		User userDetails = new User();
		sessionModel sessionmodel = new sessionModel();
		tokenmodel token_model=new tokenmodel();
		userDetails.setEmail(username);
		userDetails.setPwd(password);
		UUID idOne = UUID.randomUUID();
		sessionmodel.setSession_id(idOne.toString());
		sessionmodel.setIp_address("192.168.1.38");
		sessionmodel.setUser_id(username);
		sessionmodel.setUser_agent("userAgent");
		sessionmodel.setSession_data(username);
		session.setAttribute("session_id", idOne);
		session.setAttribute("email_id", username);
		session.setMaxInactiveInterval(20 * 60);
		
		boolean flag1=false;
		String flag="false";
		
        flag =generalUserService.getDetails(userDetails);
	
         
		logger.info("for mobile auth",flag);
		
		if (flag.equals("invalied")) {
		
			logger.info("for mobile auth2222222222",flag);
		} 
		else
		{
			logger.info("for mobile auth3333333333",flag);
			UUID tokenid = UUID.randomUUID();
			flag1=generalUserService.saveSession(sessionmodel);
			logger.info("for mobile auth44444444444",flag1);
			if (flag1) {
				
			token_model.setToken(tokenid.toString());
			token_model.setUser_id(username);
			token_model.setUser_role(flag);
			boolean flag2=generalUserService.saveToken(token_model);
			
			//boolean flag2=userDAO.saveToken(token_model);
			if (flag2) {
			System.out.println("flag2");
			
			  obj = new JSONObject();
             obj.put("username", username);
             obj.put("amount", "50");
             obj.put("token", tokenid);
             
			}
			}
		}

		  return obj.toString();
	}
	

	/**********************
	 * Invoke Admin Page
	 ***********************************/

	@RequestMapping(value = "/nanoadmin", method = RequestMethod.GET)
	public String adminindex(Model model) {
		logger.debug("adminindex()");
		return "redirect:/admin";
	}

	// list page
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String showAllUsers(Model model) {

		logger.debug("showAllUsers()");
		List<AdminModel> userList = adminService.getUserList();
		model.addAttribute("users", userList);
		return "admin/list";

	}

	// save or update user
	@RequestMapping(value = "/admin", method = RequestMethod.POST)
	public String saveOrUpdateUser(@ModelAttribute("userForm") AdminModel user, BindingResult result, Model model,
			final RedirectAttributes redirectAttributes) {

		logger.debug("saveOrUpdateUser() : {}", user);

		UserFormValidator validateadmin = new UserFormValidator();
		validateadmin.validate(user, result);
		
		if (result.hasErrors()) {
			populateDefaultModel(model);
			return "admin/userform";

		} 
		
		else {

			redirectAttributes.addFlashAttribute("css", "success");
			if (user.isNew()) {
				redirectAttributes.addFlashAttribute("msg", "User added successfully!");
			} else {
				redirectAttributes.addFlashAttribute("msg", "User updated successfully!");
			}
			adminService.updateData(user);

			return "redirect:/admin/" + user.getId();
		}

	}

	// show add user form
	@RequestMapping(value = "/admin/add", method = RequestMethod.POST)
	public String showAddUserForm(Model model) {

		logger.debug("showAddUserForm()");

		// AdminModel user = new AdminModel();
		// model.addAttribute("userForm", user);
		// populateDefaultModel(model);
		// return "admin/adduser";userform
		return "admin/signup";

	}

	// show update form
	@RequestMapping(value = "/admin/{id}/update", method = RequestMethod.GET)
	public String showUpdateUserForm(@PathVariable("id") String id, Model model) {

		logger.debug("showUpdateUserForm() : {}", id);

		AdminModel user = adminService.getuser(id);
		model.addAttribute("userForm", user);

		populateDefaultModel(model);

		return "admin/userform";

	}

	// delete user
	@RequestMapping(value = "/admin/{id}/delete", method = RequestMethod.GET)
	public String deleteUser(@PathVariable("id") String id, final RedirectAttributes redirectAttributes) {

		logger.debug("deleteUser() : {}", id);

		adminService.deleteData(id);

		redirectAttributes.addFlashAttribute("css", "success");
		redirectAttributes.addFlashAttribute("msg", "User is deleted!");

		return "redirect:/admin";

	}

	// show user
	@RequestMapping(value = "/admin/{id}", method = RequestMethod.GET)
	public String showUser(@PathVariable("id") String id, Model model) {

		logger.debug("showUser() id: {}", id);

		AdminModel user = adminService.getuser(id);
		if (user == null) {
			model.addAttribute("css", "danger");
			model.addAttribute("msg", "User not found");
		}
		model.addAttribute("user", user);

		return "admin/show";

	}

	// generate dropdown menu

	private void populateDefaultModel(Model model) {

		Map<String, String> country = new LinkedHashMap<String, String>();
		country.put("IN", "India");
		country.put("US", "United Stated");
		country.put("CN", "China");

		model.addAttribute("countryList", country);

	}

	@ExceptionHandler(EmptyResultDataAccessException.class)
	public ModelAndView handleEmptyData(HttpServletRequest req, Exception ex) {

		logger.debug("handleEmptyData()");
		logger.error("Request: {}, error ", req.getRequestURL(), ex);

		ModelAndView model = new ModelAndView();
		model.setViewName("admin/show");
		model.addObject("msg", "user not found");

		return model;

	}

	// comment notification screen

	 @RequestMapping(value = "/notification", method = RequestMethod.GET)
	 public String notification(Locale locale, Model model, HttpSession
	 session) {
	 logger.debug("notification()");
	 String currentuser =
			 generalUserService.checkUserSession(session.getAttribute("session_id").toString());
	 System.out.println("current user from database" + currentuser);
	 if (currentuser == null) {
	 return "error";
	
	 } else {
	
	logger.info("current user from session" +
	 session.getAttribute("email_id").toString());
	 if (currentuser.equals(session.getAttribute("email_id").toString())) {
	 return "notification";
	
	 } else {
	 return "users/login";
	 }
	 }
	
	 }
	 
	 
	 
	 
	 /*
	  * generating token ID
	  */
	 
	 
	 @RequestMapping(value = "/gettoken", method = RequestMethod.POST)
		public @ResponseBody String getTokenData(Model model, @RequestParam("consumerUsername") String username,
				@RequestParam("consumerToken") String Token,@RequestParam("consumerAmount") String amount, HttpSession session) {

			logger.debug("getTokenData()");
			JSONObject obj = null;
			 String merchant_id=null;
			    String consumer_id=null;
			    
			    consumer_id = trnService.checkConsumer(Token);
			  	if(username.equals(consumer_id))
						{
							System.out.println("consumer ok");
						    tokenmodel token_model=new tokenmodel();
					        UUID tokenid = UUID.randomUUID();
						    token_model.setToken(tokenid.toString());
							token_model.setUser_id(username);
							token_model.setUser_role("consumer");
							boolean flag2=generalUserService.saveToken(token_model);
							
							//boolean flag2=userDAO.saveToken(token_model);
							if (flag2) {
							System.out.println("flag2");
							
							 obj = new JSONObject();
				             obj.put("username", username);
				             obj.put("amount", "50");
				             obj.put("token", tokenid);
							
					   	}
			
			
				}
			
			

			  return obj.toString();
		}
	 
	 
	 
	 
	 

}
