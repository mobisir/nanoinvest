package com.mobisir.nanoinvest;

import java.io.IOException;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mobisir.nanoinvest.model.User;
import com.mobisir.nanoinvest.service.GeneralUserService;

public class MobileController extends HttpServlet {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Autowired
	private GeneralUserService generalUserService;

	public void setGeneralUserService(GeneralUserService generalUserService) {
		this.generalUserService = generalUserService;
	}


	private static final long serialVersionUID = 1L;
	 
    public MobileController() {
        super();
 
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	 
        response.getOutputStream().println("getting response from server");
 
    }
 
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
        try {
        	JSONObject obj=null;
           String usr = request.getParameter("email");
           String pwd = request.getParameter("pwd");
          logger.info("email"+usr+"password:"+pwd);
          
	        User userDetails = new User();
	        userDetails.setEmail(usr);
	        userDetails.setPwd(pwd);

	        boolean flag=true;
	       // boolean flag =generalUserService.getDetails(userDetails);
	        
	        logger.info(flag+"");
	        
	        response.setContentType("application/json");
	        if(flag)
	        {
	           
      	         obj = new JSONObject();
                 obj.put("token", "abdcdefghigklmn");
                 response.getWriter().write(obj.toString());
                 
	        }
	        else
	        {
	 	 	        	  obj = new JSONObject();
	 	 	        	   response.getWriter().write(obj.toString());
	     
	        }   
	        response.getWriter().write(obj.toString());
        } catch (IOException e) {
 
 
            try{
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.getWriter().print(e.getMessage());
                response.getWriter().close();
            } catch (IOException ioe) {
            }
        }   
        }
}