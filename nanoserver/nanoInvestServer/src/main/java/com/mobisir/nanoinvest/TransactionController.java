package com.mobisir.nanoinvest;

import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mobisir.nanoinvest.model.TransactionModel;
import com.mobisir.nanoinvest.model.tokenmodel;
import com.mobisir.nanoinvest.service.TransactionService;


@Controller
public class TransactionController {
	
	private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
	
	
	@Autowired
	private TransactionService transactionService;
	
	
	public void setTransactionService(TransactionService transactionService) {
		this.transactionService = transactionService;
	}

	@RequestMapping(value = "/investamount",  method = RequestMethod.POST)
	public @ResponseBody String cashDeposit(Model model,
			@RequestParam("merchantToken") String merchantToken,
			@RequestParam("consumerUsername") String consumer_userid,
			@RequestParam("consumerAmount") String consumerAmount,
			@RequestParam("consumerToken") String consumerToken,
			@RequestParam("merchantid") String merchantid,
			HttpSession session)
	{
		
		boolean flag=false;
		TransactionModel tm=new TransactionModel();
		tm.setMerchant_id(merchantid);
		tm.setConsumer_id(consumer_userid);
		tm.setAmount(consumerAmount);
		tm.setCurrency("rupee");
		
		//ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
	    //TransactionDao transactionDao = ctx.getBean("TransactionDao", TransactionDao.class);
	    
	    String merchant_id=null;
	    String consumer_id=null;
	    JSONObject obj = null;
	    
	    merchant_id=transactionService.checkMerchant(merchantToken);
	    consumer_id = transactionService.checkConsumer(consumerToken);
	    
	  
	    if (merchantid.equals(merchant_id)) {
	       System.out.println("merchant ok");
				if(consumer_userid.equals(consumer_id))
				{
					logger.info("consumer ok","in Transaction Controller");
					flag = transactionService.saveInvestment(tm);
			  	}
		  } 
	 
	    if(flag)
	    {
	    	logger.info("transaction successfull..","Transaction Controller");
	    	return "transaction_successfull";
	    	
	    }else {
			return "transaction_failed";
		}
}
	
	@RequestMapping(value = "/consumernotification",  method = RequestMethod.POST)
	public @ResponseBody String notificationTransactionConsumer(Model model,
			@RequestParam("consumerToken") String consumerToken,
			@RequestParam("consumerUsername") String consumer_userid,
			
			HttpSession session)
	{
		
		String flag=null;
		TransactionModel tm=new TransactionModel();
	    //ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
	    //TransactionDao transactionDao = ctx.getBean("TransactionDao", TransactionDao.class);
	    String merchant_id=null;
	    String consumer_id=null;
	    JSONObject obj = null;
	    consumer_id = transactionService.checkConsumer(consumerToken);
	   
	  
	    
	  	if(consumer_userid.equals(consumer_id))
				{
	  		
					System.out.println("consumer ok"+consumer_userid+consumer_id);
					flag = transactionService.GetNotificationConsumer(consumer_id);
					
			   	}
	   
	   return flag;

		  
	}
	
	
	
	@RequestMapping(value = "/merchantnotification",  method = RequestMethod.POST)
	public @ResponseBody String notificationTransactionMerchant(Model model,
			@RequestParam("merchantToken") String merchantToken,
			@RequestParam("merchantid") String merchantid,
			
			HttpSession session)
	{
		
		String flag=null;
		
		TransactionModel tm=new TransactionModel();
	   // ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
	    //TransactionDao transactionDao = ctx.getBean("TransactionDao", TransactionDao.class);
	    String merchant_id=null;
	    String consumer_id=null;
	    JSONObject obj = null;
	    merchant_id = transactionService.checkMerchant(merchantToken);
	   
	    if (merchantid.equals(merchant_id)) {
	      
					System.out.println("merchant ok"+merchantid+merchant_id);
//					flag = transactionService.GetNotificationConsumer(merchant_id);
					flag = transactionService.GetNotificationMerchant(merchant_id);
				
	    } 
	   return flag;

		  
	}
	
	
	
	
	
		
	
}
