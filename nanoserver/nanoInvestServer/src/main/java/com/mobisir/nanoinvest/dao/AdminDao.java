package com.mobisir.nanoinvest.dao;

import java.util.List;

import com.mobisir.nanoinvest.model.AdminModel;

/*
 * Interface for Admin database operation
 */

public interface AdminDao {
	

	//insert into database user_master
	public void insertData(AdminModel user); 
	
	//fetch user details
	public List<AdminModel> getUserList(); 
	
	//delete user details
	public void deleteData(String id); 
	
	//update user details
    public void updateData(AdminModel user);  
    
    //fetch user by emailid
    public AdminModel getUser(String id);
    
    //public void save(AdminModel userModel);

}
