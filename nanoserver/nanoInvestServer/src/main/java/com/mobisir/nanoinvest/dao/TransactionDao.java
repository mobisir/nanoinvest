package com.mobisir.nanoinvest.dao;

import com.mobisir.nanoinvest.model.TransactionModel;


public interface TransactionDao {

	public String checkMerchant(String token_id);
	
	public String checkConsumer(String token_id);

	public boolean saveInvestment(TransactionModel tm);
	
	public String GetNotificationMerchant(String token_id);

	public String GetNotificationConsumer(String token_id);
}

