package com.mobisir.nanoinvest.dao;

import com.mobisir.nanoinvest.model.AdminModel;
import com.mobisir.nanoinvest.model.AuthenticationResultBean;
import com.mobisir.nanoinvest.model.User;
import com.mobisir.nanoinvest.model.sessionModel;
import com.mobisir.nanoinvest.model.tokenmodel;

/*
 * Interface for user database operation
 */

public interface UserDao {
	
	public String getDetails(User id);

	public boolean saveSession(sessionModel id);

	public String checkUserSession(String id);

	public void deleteSession();
	
	public AuthenticationResultBean AuthenticateUser(AdminModel user);

	public void save(AdminModel userModel);
	
	public void update(AdminModel user);
	 public boolean saveToken(tokenmodel tm);
}


