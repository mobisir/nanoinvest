package com.mobisir.nanoinvest.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.mysql.jdbc.Statement;
import com.mobisir.nanoinvest.dao.TransactionDao;
import com.mobisir.nanoinvest.model.TransactionModel;

public class TransactionDaoImpl implements TransactionDao{

	@Autowired
	DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public String checkMerchant(String token_id) {
		// TODO Auto-generated method stub
	
		System.out.println("entered user name" + token_id);
		Connection con = null;
        String user_id = null;
		try {
			con = dataSource.getConnection();
			Statement st = (Statement) con.createStatement();
			String sql = "SELECT user_id FROM token_master WHERE token = \"" + token_id + "\" and user_role=\"merchant\";";
			System.out.println("Prepared Query" + sql);
			ResultSet rs = st.executeQuery(sql);
			if (rs.next()) {

				user_id = rs.getString(1);
			
		    }
			con.close();
			
		}catch (Exception e) {

			System.out.println("database java" + e);

		} 
		
		return user_id;
		
	}

	@Override
	public String checkConsumer(String token_id) {
		// TODO Auto-generated method stub
		//select * from
				System.out.println("entered user name" + token_id);
				Connection con = null;
		        String user_id = null;
				try {
					con = dataSource.getConnection();
					Statement st = (Statement) con.createStatement();
					String sql = "SELECT user_id FROM token_master WHERE token = \"" + token_id + "\" and user_role=\"consumer\";";
					System.out.println("Prepared Query" + sql);
					ResultSet rs = st.executeQuery(sql);
					if (rs.next()) {

						user_id = rs.getString(1);
					
				    }
					con.close();
					
				}catch (Exception e) {

					System.out.println("database java" + e);

				} 
				
				return user_id;
	}

	public boolean saveInvestment(TransactionModel tm) {
		// TODO Auto-generated method stub
		Connection con = null;

		boolean tag = false;
		try {

			con = dataSource.getConnection();

			Statement st = (Statement) con.createStatement();
			String sql = "insert into transaction_master values(null,(select user_id from users_master where user_email=\"" + tm.getConsumer_id() + "\"),(select user_id from users_master where user_email=\"" + tm.getMerchant_id() + "\"),'"
					+ tm.getAmount() + "','" + tm.getCurrency() + "',now())";

			System.out.println("Prepared Query" + sql);
			int i = st.executeUpdate(sql);
			if (i > 0) {
				tag = true;
			}
		} catch (Exception e) {

			System.out.println("database java" + e);

		} finally {

			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tag;
		
	}
	


	@Override
	public String GetNotificationConsumer(String token_id) {
		// TODO Auto-generated method stub
		System.out.println("entered user name...." + token_id);
		Connection con = null;
        String user_id = null;
     	 JSONArray jsonArray = new JSONArray();
		try {
			con = dataSource.getConnection();
			Statement st = (Statement) con.createStatement();
//			String sqlm = "SELECT amount,(select user_fname from users_master where user_id=consumer_id) as fname,DATE_FORMAT(transaction_time, '%d %m %Y') as transaction_time1 FROM transaction_master WHERE merchant_id =(select user_id from users_master where user_email=\"" + token_id + "\");
			String sql = "SELECT amount,(select user_fname from users_master where user_id=merchant_id) as fname,DATE_FORMAT(transaction_time, '%d %m %Y') as transaction_time1 FROM transaction_master WHERE consumer_id =(select user_id from users_master where user_email=\"" + token_id + "\");";
			System.out.println("Prepared Query" + sql);
			
			ResultSet rs = st.executeQuery(sql);
		
		        while (rs.next()) {
		            int total_rows = rs.getMetaData().getColumnCount();
		            JSONObject obj = new JSONObject();
		                 obj.put("amount", rs.getString("amount"));
			             obj.put("transactionTime", rs.getString("transaction_time1"));
			             obj.put("emailid", rs.getString("fname"));
		                 jsonArray.add(obj);
		          }
			con.close();
			
		}catch (Exception e) {

			System.out.println("database java" + e);

		} 
		System.out.println("SSSSSSS"+jsonArray.toJSONString());
		return jsonArray.toJSONString();
	}

	@Override
	public String GetNotificationMerchant(String token_id) {
		// TODO Auto-generated method stub

		System.out.println("entered merchant name....");
		Connection con = null;
        //String user_id = null;
        JSONArray jsonArray = new JSONArray();
		try {
			con = dataSource.getConnection();
			Statement st = (Statement) con.createStatement();
			String sql = "SELECT amount,(select user_fname from users_master where user_id=consumer_id) as fname,DATE_FORMAT(transaction_time, '%d %m %Y') as transaction_time1 FROM transaction_master WHERE merchant_id =(select user_id from users_master where user_email=\"" + token_id + "\");";
			System.out.println("Prepared Query" + sql);
			ResultSet rs = st.executeQuery(sql);

	        while (rs.next()) {
	            int total_rows = rs.getMetaData().getColumnCount();
	            JSONObject obj = new JSONObject();
	            
	       	 obj.put("amount", rs.getString("amount"));
             obj.put("transactionTime", rs.getString("transaction_time1"));
             obj.put("emailid", rs.getString("fname"));
	                jsonArray.add(obj);
	           
	        }
			con.close();
			
		}catch (Exception e) {

			System.out.println("database java" + e);

		} 

		System.out.println("SSSSSSS"+jsonArray.toJSONString());
		return jsonArray.toJSONString();
	}


}
