package com.mobisir.nanoinvest.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.mobisir.nanoinvest.dao.UserDao;
import com.mobisir.nanoinvest.jdbc.UserMapper;
import com.mobisir.nanoinvest.model.AdminModel;
import com.mobisir.nanoinvest.model.AuthenticationResultBean;
import com.mobisir.nanoinvest.model.User;
import com.mobisir.nanoinvest.model.sessionModel;
import com.mobisir.nanoinvest.model.tokenmodel;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

/*
 * Database handler class for User
 */


public class UserDAOImpl implements UserDao {
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);
	
	@Override
	public AuthenticationResultBean AuthenticateUser(AdminModel user){
		
		AuthenticationResultBean authresult=new AuthenticationResultBean();
		
		try {
			
			List<AdminModel> userList = new ArrayList<AdminModel>();
			
			String sql = "SELECT * FROM users_master WHERE user_email = \"" + user.getEmail() + "\" AND user_password =\""+user.getPwd()+"\";";
			logger.info(sql);
			

			JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
			userList = jdbcTemplate.query(sql, new UserMapper());
			
			if(!userList.isEmpty()){
				
				authresult.setRole(userList.get(0).getRole());
				authresult.setLoginSuccess(true);

				
			}else{
				
				authresult.setLoginSuccess(false);
				
			}
		} catch (Exception e) {

			logger.error("SQL error in user database"+e);

		}
		return authresult;
		
	}
	
	
	@Override
	public String getDetails(User user) {
		
		// TODO Auto-generated method stub
				logger.info("In UserDAo impl","entered user name" + user.getEmail());
				
				Connection con = null;
				String userrole=null;

				boolean tag = false;
				try {

					con = dataSource.getConnection();
					Statement st = (Statement) con.createStatement();
					String sql = "SELECT user_password,user_role FROM users_master WHERE user_email = \"" + user.getEmail() + "\";";
					System.out.println("Prepared Query" + sql);
					
					
					ResultSet rs = st.executeQuery(sql);
					
					if (rs != null) {
						try {
							while (rs.next()) {

								String pwd = rs.getString("user_password");
								userrole = rs.getString("user_role");
								logger.info("get details","pwd from database" + pwd);
								if (pwd.equals(user.getPwd())) {
									
								
									// redirect to success page
									 tag = true;

								} else {
									logger.info("error","password wrong");
									// throw error page
									tag = false;

								}
							}
						} catch (Exception e) {

							logger.error(""+e);

						} finally {

							rs.close();
							con.close();
						}
					} else {
						logger.info("","dataset null");
						// throw error page
						 tag = false;

					}
				} catch (SQLException e) {
					logger.error(e+"");

				}
			  
				if(tag)
				{
					return userrole;
				}
				else {
					return "invalied";
				}
				
	}

	public boolean saveSession(sessionModel sm) {
		// TODO Auto-generated method stub
		Connection con = null;

		boolean tag = false;
		
		try {

			con = dataSource.getConnection();

			Statement st = (Statement) con.createStatement();
			String sql = "insert into user_sessions values('" + sm.getSession_id() + "','" + sm.getIp_address() + "','"
					+ sm.getUser_agent() + "',123,'" + sm.getSession_data() + "',now(),now())";

			logger.info(sql);
			int i = st.executeUpdate(sql);
			if (i > 0) {
				tag = true;
			}
		} catch (Exception e) {

			logger.error(""+e);

		} finally {

			try {
				con.close();
			} catch (SQLException e) {
				logger.error("SQL error in user database"+e);
			}

		}
		
		logger.info("**************","tag in sesion model..........."+tag);
		return tag;

	}

	@Override
	public String checkUserSession(String id) {
		// TODO Auto-generated method stub

		Connection con = null;
		String currentuser = null;
		try {

			con = dataSource.getConnection();

			Statement st = (Statement) con.createStatement();
			String sql = "SELECT user_data FROM user_sessions WHERE session_id = \"" + id + "\";";

			logger.info(sql);

			ResultSet rs = st.executeQuery(sql);

			if (rs.next()) {

				currentuser = rs.getString(1);
			}
		} catch (Exception e) {

			logger.error(""+e);

		} finally {

			try {
				con.close();
			} catch (SQLException e) {
				
				logger.error("SQL error in user database"+e);
			}

		}

		return currentuser;

	}

	// Delete previos sessions
	@Override
	public void deleteSession() {
		// TODO Auto-generated method stub
		Connection con = null;
		try {

			con = dataSource.getConnection();

			Statement st = (Statement) con.createStatement();
			String sql = "DELETE FROM user_sessions WHERE session_updated < (NOW() - INTERVAL 1 DAY)";

			logger.info("Prepared Query" + sql);
			st.executeUpdate(sql);

		} catch (Exception e) {
			
			logger.error("" + e);
		
		} finally {

			try {
				con.close();
			} catch (SQLException e) {
				logger.error("SQLException in delete Session" + e);
			}

		}

	}
	
	@Override
	public void save(AdminModel userModel) {
		// TODO Auto-generated method stub
		try {
				String profilequery = "INSERT INTO users_master(user_fname,user_lname,user_email,user_password,user_ph,user_role,user_dob,user_address,user_city,user_state,user_sec_qus,user_sec_ans) values (?,?,?,?,?,?,?,?,?,?,?,?)";
				logger.info(profilequery);
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				Connection con = (Connection) jdbcTemplate.getDataSource().getConnection();
				PreparedStatement ps = null;
				
					con = (Connection) dataSource.getConnection();
					ps = (PreparedStatement) con.prepareStatement(profilequery);
					ps.setString(1, userModel.getFname());
					ps.setString(2, userModel.getLname());
					ps.setString(3, userModel.getEmail());
					ps.setString(4, userModel.getPwd());
					ps.setString(5, userModel.getPh());
					ps.setString(6, userModel.getRole());
					ps.setString(7, userModel.getDob());
					ps.setString(8, userModel.getAddress());
					ps.setString(9, userModel.getCity());
					ps.setString(10, userModel.getState());
					ps.setString(11, userModel.getQues());
					ps.setString(12, userModel.getAns());
					int userId = ps.executeUpdate(profilequery, ps.RETURN_GENERATED_KEYS);
					
					logger.info("Autogerated userId" + userId);
					String email= userModel.getEmail();
					if (userId != 0) {
						
						logger.info("user saved with id=" + email);
						
						//String bankquery="INSERT INTO users_master(user_id,user_bank_name,user_bank_acc,user_bank_branch,user_ifsc,user_bank_address,user_bank_city,user_bank_state) values (?,?,?,?,?,?,?,?)";
					    //logger.info(bankquery);
						//jdbcTemplate.execute(bankquery);
						
					} else {
						 
						logger.info("user save failed with id=" + email);
					}

				} catch (SQLException e) {
					logger.error("SQL error in user database"+e);
				} 
			}


	@Override
	public void update(AdminModel user) {
		// TODO Auto-generated method stub
		
		String sql = "UPDATE users_master set useruser_password = ?, user_address = ?,user_state=? where user_id = ?";
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		jdbcTemplate.update(
				sql,
				new Object[] { user.getPwd(), user.getAddress(),user.getState(),user.getId() });
		
	}


	@Override
	public boolean saveToken(tokenmodel token_m) {
		// TODO Auto-generated method stub
		Connection con = null;

		boolean tag = false;
		try {

			con = dataSource.getConnection();

			Statement st = (Statement) con.createStatement();
			String sql = "insert into token_master values(null,'" + token_m.getToken() + "','"
					+ token_m.getUser_id() + "','" + token_m.getUser_role() + "',now())";

		    logger.info("","Prepared Query" + sql);
			int i = st.executeUpdate(sql);
			if (i > 0) {
				tag = true;
			}
		} catch (Exception e) {

			logger.info("UserDaoImpl" + e);

		} finally {

			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				logger.error("erron in save token"+e);
			}

		}
		
		return tag;
	}
	
	
	
	

}
