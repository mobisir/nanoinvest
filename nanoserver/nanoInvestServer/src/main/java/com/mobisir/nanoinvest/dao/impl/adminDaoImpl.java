package com.mobisir.nanoinvest.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import com.mobisir.nanoinvest.dao.AdminDao;
import com.mobisir.nanoinvest.jdbc.UserMapper;
import com.mobisir.nanoinvest.model.AdminModel;

/*
 * Database handler class for Admin
 */

public class adminDaoImpl implements AdminDao {

	private static final Logger logger = LoggerFactory.getLogger(adminDaoImpl.class);
	
	@Autowired
	DataSource dataSource;
	

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public void insertData(AdminModel user) {
		// TODO Auto-generated method stub

		String sql = "INSERT INTO users_master "
				+ "((user_fname,user_lname,user_email,user_password,user_ph,user_role,user_dob,user_address,user_city,user_state,user_sec_qus,user_sec_ans) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		logger.info("insert queary for admin..",sql);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		jdbcTemplate.update(sql,
				new Object[] { user.getFname(), user.getLname(), user.getEmail(), user.getPwd(), user.getPh(),
						user.getRole(), user.getDob(), user.getAddress(), user.getCity(), user.getState(),
						user.getQues(), user.getAns() });

	}

	@Override
	public List<AdminModel> getUserList() {
		// TODO Auto-generated method stub
		List<AdminModel> userList = new ArrayList<AdminModel>();
		userList=null;

		String sql = "select * from users_master";
		logger.info("getuserslist queary for admin..",sql);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		userList = jdbcTemplate.query(sql, new UserMapper());
		return userList;
	}

	@Override
	public void deleteData(String id) {
		// TODO Auto-generated method stub

		String sql = "delete from users_master where user_id=" + id;
		logger.info("delete queary for admin..",sql);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.update(sql);

	}

	@Override
	public void updateData(AdminModel user) {
		// TODO Auto-generated method stub

		String sql = "UPDATE users_master set user_password = ?, user_address = ?,user_state=? where user_id = ?";
		
		logger.info("update queary for admin.."+sql+"update");
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		jdbcTemplate.update(sql, new Object[] { user.getPwd(), user.getAddress(),
				 user.getState(), user.getId() });

	}

	@Override
	public AdminModel getUser(String id) {
		// TODO Auto-generated method stub
		List<AdminModel> userList = new ArrayList<AdminModel>();
		String sql = "select * from users_master where user_id= " + id;
		logger.info("getUser for adminDaoimpl",sql);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		userList = jdbcTemplate.query(sql, new UserMapper());
		return userList.get(0);
	}

}
