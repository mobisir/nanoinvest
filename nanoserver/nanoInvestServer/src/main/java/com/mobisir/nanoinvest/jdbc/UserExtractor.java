package com.mobisir.nanoinvest.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import com.mobisir.nanoinvest.model.AdminModel;

public class UserExtractor implements ResultSetExtractor<AdminModel> {

	public AdminModel extractData(ResultSet rs) throws SQLException, DataAccessException {

		AdminModel user = new AdminModel();

		user.setId(rs.getInt("user_id"));
		user.setFname(rs.getString("user_fname"));
		user.setLname(rs.getString("user_lname"));
		user.setEmail(rs.getString("user_email"));
		//user.setPwd(rs.getString("user_password"));
		user.setPh(rs.getString("user_ph"));
		user.setRole(rs.getString("user_role"));
		
		//user.setDob(rs.getString("user_dob"));
		//user.setAddress(rs.getString("user_address"));
		//user.setCity(rs.getString("user_city"));
		// user.setState(rs.getString("state"));

		// user.setBankname(rs.getString("bankname"));
		// user.setBankac(rs.getString("bankac"));
		// user.setBankbr(rs.getString("bankbr"));

		return user;
	}
}