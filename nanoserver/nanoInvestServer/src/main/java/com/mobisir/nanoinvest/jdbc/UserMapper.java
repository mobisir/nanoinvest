package com.mobisir.nanoinvest.jdbc;
import java.sql.ResultSet;  
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.mobisir.nanoinvest.model.AdminModel;

 
public class UserMapper  implements RowMapper<AdminModel> { 
	

	public AdminModel mapRow(ResultSet resultSet, int line) throws SQLException {
		
		
		UserExtractor userExtractor = new UserExtractor();  
		return userExtractor.extractData(resultSet);  
		  
		
		
	}
 
}    