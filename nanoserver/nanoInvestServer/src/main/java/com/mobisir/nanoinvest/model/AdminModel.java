package com.mobisir.nanoinvest.model;

public class AdminModel {
	Integer id=-1;
	String fname="";
	String lname="";
	String email="";
	String pwd="";
	String ConfirmPassword="";
	String ph="";
	String role="";
	String dob="";
	String address="";
	String city="";
    String state="";
    String ques="";
    String ans="";
    String bankac="";
    String bankname="";
    String bankifsc="";
    String bankbr="";
    String bankaddr="";
    String bankcity="";
    String bankstate="";
    
    public boolean isNew() {
		return (this.id == null);
	} 
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getConfirmPassword() {
		return ConfirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		ConfirmPassword = confirmPassword;
	}
	public String getPh() {
		return ph;
	}
	public void setPh(String ph) {
		this.ph = ph;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getQues() {
		return ques;
	}
	public void setQues(String ques) {
		this.ques = ques;
	}
	public String getAns() {
		return ans;
	}
	public void setAns(String ans) {
		this.ans = ans;
	}
	public String getBankac() {
		return bankac;
	}
	public void setBankac(String bankac) {
		this.bankac = bankac;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getBankifsc() {
		return bankifsc;
	}
	public void setBankifsc(String bankifsc) {
		this.bankifsc = bankifsc;
	}
	public String getBankbr() {
		return bankbr;
	}
	public void setBankbr(String bankbr) {
		this.bankbr = bankbr;
	}
	public String getBankaddr() {
		return bankaddr;
	}
	public void setBankaddr(String bankaddr) {
		this.bankaddr = bankaddr;
	}
	public String getBankcity() {
		return bankcity;
	}
	public void setBankcity(String bankcity) {
		this.bankcity = bankcity;
	}
	public String getBankstate() {
		return bankstate;
	}
	public void setBankstate(String bankstate) {
		this.bankstate = bankstate;
	}
	
	@Override
	public String toString() {
		return "adminDao [id=" + id + ", fname=" + fname + ", lname=" + lname + ", email=" + email + ", pwd=" + pwd
				+ ", ConfirmPassword=" + ConfirmPassword + ", ph=" + ph + ", role=" + role + ", dob=" + dob
				+ ", address=" + address + ", city=" + city + ", state=" + state + ", ques=" + ques + ", ans=" + ans
				+ "]"+ isNew();
	}

    
    
}
