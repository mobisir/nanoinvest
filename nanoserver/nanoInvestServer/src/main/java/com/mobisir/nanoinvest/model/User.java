package com.mobisir.nanoinvest.model;

public class User {

   
	private String email;
	
	private String pwd;
	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	private String userrole;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	

	@Override
	public String toString() {
		return "User [email=" + email + ", password=" + pwd + "]";
	}

	public String getUserrole() {
		return userrole;
	}

	public void setUserrole(String userrole) {
		this.userrole = userrole;
	}

	
}
