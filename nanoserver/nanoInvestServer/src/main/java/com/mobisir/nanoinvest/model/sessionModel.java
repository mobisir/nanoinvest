package com.mobisir.nanoinvest.model;

public class sessionModel {

	private String session_id;
	private String ip_address;
    private String user_id;
    private String user_agent;
    private String last_activity;
    private String session_data;
    
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public String getUser_agent() {
		return user_agent;
	}
	public void setUser_agent(String user_agent) {
		this.user_agent = user_agent;
	}
	public String getLast_activity() {
		return last_activity;
	}
	public void setLast_activity(String last_activity) {
		this.last_activity = last_activity;
	}
	public String getSession_id() {
		return session_id;
	}
	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getSession_data() {
		return session_data;
	}
	public void setSession_data(String session_data) {
		this.session_data = session_data;
	}
    
    
    
}
