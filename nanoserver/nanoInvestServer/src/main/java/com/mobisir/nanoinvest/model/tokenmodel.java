package com.mobisir.nanoinvest.model;

public class tokenmodel {
	private String token_id;
	private String token;
    private String user_id;
    private String user_role;
    private String token_created;
	public String getToken_id() {
		return token_id;
	}
	public void setToken_id(String token_id) {
		this.token_id = token_id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_role() {
		return user_role;
	}
	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}
	public String getToken_created() {
		return token_created;
	}
	public void setToken_created(String token_created) {
		this.token_created = token_created;
	}
    
}
