package com.mobisir.nanoinvest.service;

import com.mobisir.nanoinvest.model.AdminModel;
import com.mobisir.nanoinvest.model.AuthenticationResultBean;
import com.mobisir.nanoinvest.model.User;
import com.mobisir.nanoinvest.model.sessionModel;
import com.mobisir.nanoinvest.model.tokenmodel;

public interface GeneralUserService {
	
	public String getDetails(User id);

	public boolean saveSession(sessionModel id);

	public String checkUserSession(String id);

	public void deleteSession();

	public AuthenticationResultBean AuthenticateUser(AdminModel user);
	
	 public void save(AdminModel userModel);
	 
	 public boolean saveToken(tokenmodel tm);
	
}
