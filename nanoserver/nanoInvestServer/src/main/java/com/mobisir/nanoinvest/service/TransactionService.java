package com.mobisir.nanoinvest.service;

import com.mobisir.nanoinvest.model.TransactionModel;

public interface TransactionService {

	public String checkMerchant(String token_id);

	public String checkConsumer(String token_id);

	public boolean saveInvestment(TransactionModel tm);

	public String GetNotificationMerchant(String token_id);

	public String GetNotificationConsumer(String token_id);

}
