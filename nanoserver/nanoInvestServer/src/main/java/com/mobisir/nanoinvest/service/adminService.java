package com.mobisir.nanoinvest.service;

import java.util.List;

import com.mobisir.nanoinvest.model.AdminModel;



public interface adminService {
	
	 public void insertData(AdminModel user);  
	 public List<AdminModel> getUserList();  
	 public void deleteData(String id);  
	 public AdminModel getuser(String id);  
	 public void updateData(AdminModel user);  
	 

}
