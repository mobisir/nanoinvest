package com.mobisir.nanoinvest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobisir.nanoinvest.dao.UserDao;
import com.mobisir.nanoinvest.model.AdminModel;
import com.mobisir.nanoinvest.model.AuthenticationResultBean;
import com.mobisir.nanoinvest.model.User;
import com.mobisir.nanoinvest.model.sessionModel;
import com.mobisir.nanoinvest.model.tokenmodel;
import com.mobisir.nanoinvest.service.GeneralUserService;

@Service("generalUserService")
public class GeneralUserServiceImpl implements GeneralUserService{

	
	@Autowired
	UserDao userDAO;
	
	@Override
	public String getDetails(User id) {
		// TODO Auto-generated method stub
		
		return userDAO.getDetails(id);
		
		
	}

	@Override
	public boolean saveSession(sessionModel id) {
		// TODO Auto-generated method stub
		return userDAO.saveSession(id);
	}

	@Override
	public String checkUserSession(String id) {
		// TODO Auto-generated method stub
		return userDAO.checkUserSession(id);
	}

	@Override
	public void deleteSession() {
		userDAO.deleteSession();
		
	}

	@Override
	public AuthenticationResultBean AuthenticateUser(AdminModel user) {
		// TODO Auto-generated method stub
		
		
		return userDAO.AuthenticateUser(user);
	}

	@Override
	public void save(AdminModel userModel) {
		
		userDAO.save(userModel);
		
	}

	@Override
	public boolean saveToken(tokenmodel tm) {
		// TODO Auto-generated method stub
		return userDAO.saveToken(tm);
	}

	
	
	

}
