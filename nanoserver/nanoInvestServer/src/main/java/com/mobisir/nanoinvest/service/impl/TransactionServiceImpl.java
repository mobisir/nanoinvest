package com.mobisir.nanoinvest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobisir.nanoinvest.dao.TransactionDao;
import com.mobisir.nanoinvest.model.TransactionModel;
import com.mobisir.nanoinvest.service.TransactionService;

@Service("transactionService")
public class TransactionServiceImpl implements TransactionService{
	
	@Autowired
	TransactionDao transactionDao;
	

	public void setTransactionDao(TransactionDao transactionDao) {
		this.transactionDao = transactionDao;
	}

	@Override
	public String checkMerchant(String token_id) {
		// TODO Auto-generated method stub
		return transactionDao.checkMerchant(token_id);
	}

	@Override
	public String checkConsumer(String token_id) {
		// TODO Auto-generated method stub
		return transactionDao.checkConsumer(token_id);
	}

	@Override
	public boolean saveInvestment(TransactionModel tm) {
		// TODO Auto-generated method stub
		return transactionDao.saveInvestment(tm);
	}

	@Override
	public String GetNotificationMerchant(String token_id) {
		// TODO Auto-generated method stub
		return transactionDao.GetNotificationMerchant(token_id);
	}

	@Override
	public String GetNotificationConsumer(String token_id) {
		// TODO Auto-generated method stub
		return transactionDao.GetNotificationConsumer(token_id);
	}

}
