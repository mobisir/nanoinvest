package com.mobisir.nanoinvest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mobisir.nanoinvest.dao.AdminDao;
import com.mobisir.nanoinvest.model.AdminModel;
import com.mobisir.nanoinvest.service.adminService;

@Service("userService")
public class adminServiceImpl implements adminService  {

	
	@Autowired
	AdminDao adminDao;
	
	@Override
	public void insertData(AdminModel user) {
		// TODO Auto-generated method stub
		
		adminDao.insertData(user);
		
	}

	@Override
	public List<AdminModel> getUserList() {
		// TODO Auto-generated method stub
		return adminDao.getUserList();
	}

	@Override
	public void deleteData(String id) {
		// TODO Auto-generated method stub
		adminDao.deleteData(id);
	}

	@Override
	public AdminModel getuser(String id) {
		// TODO Auto-generated method stub
		return adminDao.getUser(id);
	}

	@Override
	public void updateData(AdminModel user) {
		// TODO Auto-generated method stub
		
		adminDao.updateData(user);
		
	}

}
