package com.mobisir.nanoinvest.utils;

import java.util.Random;

//to generate random number
public class GenerateNanoToken {
	
	public static String getRandomToken(){
		
		  Random random = new Random();
		    return random.nextInt((NanoConstants.RANDOM_MAX - NanoConstants.RANDOM_MIN) + 1) + NanoConstants.RANDOM_MIN+"";
	}
	
}
