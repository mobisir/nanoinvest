package com.mobisir.nanoinvest.utils;

public class NanoConstants {

	public static final String MERCHANT="2";
	public static final String ADMIN="1";
	public static final String CONSUMER="3";
	public static final Integer RANDOM_MAX=999999999;
	public static final Integer RANDOM_MIN=111111111;
}
