package com.mobisir.nanoinvest.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.mobisir.nanoinvest.model.AdminModel;
import com.mobisir.nanoinvest.service.adminService;


/* 
 * validate user form
 */

@Component
public class UserFormValidator implements Validator {

	@Autowired
	
	@Qualifier("emailValidator")
	EmailValidator emailValidator;

	
	@Override
	public boolean supports(Class<?> clazz) {
		return AdminModel.class.equals(clazz);
	}

	
	@Override
	public void validate(Object target, Errors errors) {
		
	
		AdminModel user = (AdminModel) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fname", "NotEmpty.userForm.name");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.userForm.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "NotEmpty.userForm.address");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pwd", "NotEmpty.userForm.password");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword","NotEmpty.userForm.confirmPassword");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "state", "NotEmpty.userForm.country");

//		if(!emailValidator.valid(user.getEmail())){
//			errors.rejectValue("email", "Pattern.userForm.email");
//		}
		
		if(user.getState().equalsIgnoreCase("none")){
			errors.rejectValue("state", "NotEmpty.userForm.country");
		}
		
		if (!user.getPwd().equals(user.getConfirmPassword())) {
			errors.rejectValue("confirmPassword", "Diff.userform.confirmPassword");
		}
		
	}
	}