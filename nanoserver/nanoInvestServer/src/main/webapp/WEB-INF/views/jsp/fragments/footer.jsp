<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="container">
	<hr>
	<footer>
	<spring:message code="label.footer"/>

	</footer>
</div>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<spring:url value="/resources/nanothemes/js/hello.js" var="coreJs" />
<spring:url value="/resources/nanothemes/js/bootstrap.min.js"
	var="bootstrapJs" />

<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>


