<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<head>
<!-- <title>Nano Admin sample Code</title> -->
<h3><spring:message code="label.title"/></h3>

<spring:url value="/resources/nanothemes/css/hello.css" var="coreCss" />
<spring:url value="/resources/nanothemes/css/bootstrap.min.css"
	var="bootstrapCss" />
<link href="${bootstrapCss}" rel="stylesheet" />
<link href="${coreCss}" rel="stylesheet" />
</head>

<spring:url value="/" var="urlHome" />
<spring:url value="admin/add" var="urlAddUser" />

<nav class="navbar navbar-inverse ">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="${urlHome}">Admin update from for User</a>
		</div>
		<%-- <div id="navbar">
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="${urlAddUser}">Add User</a></li>
			</ul>
		</div> --%>
	</div>
</nav>